package com.sothebys.eos.client;

import com.sothebys.eos.client.repository.support.CustomRepositoryFactoryBean;
import com.sothebys.eos.client.repository.support.CustomRepositoryImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableAutoConfiguration
@EnableSwagger2
@EnableJpaRepositories(
    repositoryBaseClass = CustomRepositoryImpl.class,
    repositoryFactoryBeanClass = CustomRepositoryFactoryBean.class
)
@EnableAsync
@SpringBootApplication
public class ClientServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(ClientServiceApplication.class, args);
  }
}
