package com.sothebys.eos.client.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.sothebys.eos.client.dto.SnsMessageDto;
import com.sothebys.eos.client.dto.SnsMessageOperation;
import com.sothebys.eos.client.exception.ClientNotFoundException;
import com.sothebys.eos.client.exception.ClientServiceException;
import com.sothebys.eos.client.exception.MessageServiceException;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.model.QClient;
import com.sothebys.eos.client.repository.ClientRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

  private final ClientRepository clientRepository;
  private final QClient clientQuery = QClient.client;

  private final MessageService messageService;

  private final ObjectMapper objectMapper;


  @Value("${sns.arn.create-client}")
  private String snsArnCreateClient;

  @Value("${sns.arn.update-client}")
  private String snsArnUpdateClient;

  @Value("${sns.arn.delete-client}")
  private String snsArnDeleteClient;

  /**
   * Constructor.
   */
  @Autowired
  public ClientServiceImpl(ClientRepository clientRepository, MessageService messageService,
      @Lazy ObjectMapper objectMapper) {
    this.clientRepository = clientRepository;
    this.messageService = messageService;
    this.objectMapper = objectMapper;
  }


  @Override
  public Client findById(String id) {
    if (id == null) {
      throw new ClientNotFoundException(id);
    }

    final Client storedClient = clientRepository.findOne(clientQuery.filterEntityById(id));

    if (storedClient == null) {
      throw new ClientNotFoundException(id);
    }

    return storedClient;
  }


  @Override
  @Transactional
  public Client create(Client client) {
    if (client.getMainClient() == null) {
      client.setMainClient(client);
    }

    final Client createdClient;

    if (StringUtils.isBlank(client.getEntityId())) {
      createdClient = clientRepository.insertWithIdGeneration(client);
    } else {
      createdClient = clientRepository.save(client);
    }

    if (createdClient != null) {
      SnsMessageDto msgDto = new SnsMessageDto();
      msgDto.setId(createdClient.getEntityId());
      try {
        messageService.sendMessage(objectMapper.writeValueAsString(msgDto), snsArnCreateClient,
            SnsMessageOperation.CREATE);
      } catch (Exception e) {
        throw new MessageServiceException("Error sensing message to AWS SNS on Create Operation "
            + "for Client with ID: " + msgDto.getId());
      }
    }
    return createdClient;
  }

  @Override
  @Transactional
  public Client update(Client client) {
    if (client.getMainClient() == null) {
      client.setMainClient(client);
    }
    final Client updatedClient = clientRepository.save(client);

    return findById(updatedClient.getEntityId());
  }


  @Override
  @Transactional
  public Client save(Client client) {
    Optional<Client> existingClientOptional = clientRepository
        .findByEntityId(client.getEntityId());

    existingClientOptional.ifPresent(existingClient -> {
      client.setEntityId(existingClient.getEntityId());

      if (client.getLastSyncDate() != null && existingClient.getLastSyncDate() != null
          && client.getLastSyncDate().compareTo(existingClient.getLastSyncDate()) < 0) {
        throw new ClientServiceException(String.format(
            "Client was not saved because the request date (%s) is before the stored "
                + "lastSyncDate (%s)", client.getLastSyncDate(), existingClient.getLastSyncDate()));
      }
    });

    if (client.getMainClient() == null) {
      client.setMainClient(client);
    }

    Client savedClient = clientRepository.save(client);

    if (savedClient != null) {
      SnsMessageDto msgDto = new SnsMessageDto();
      msgDto.setId(savedClient.getEntityId());
      try {
        messageService.sendMessage(objectMapper.writeValueAsString(msgDto), snsArnCreateClient,
            SnsMessageOperation.CREATE);
      } catch (Exception e) {
        throw new MessageServiceException("Error sensing message to AWS SNS on Create Operation "
            + "for Client with ID: " + msgDto.getId());
      }
    }

    return savedClient;
  }


  @Override
  public Page<Client> findAllIdOnly(Predicate predicate, Pageable pageable) {
    Predicate augmentedPredicate = new BooleanBuilder(predicate).getValue();
    QClient client = QClient.client;
    return clientRepository.findAll(Projections.bean(Client.class, client.entityId),
        augmentedPredicate, pageable);
  }

  @Override
  public Page<Client> findAll(Predicate predicate, Pageable pageable) {
    Predicate augmentedPredicate = new BooleanBuilder(predicate).getValue();
    return clientRepository.findAll(augmentedPredicate, pageable);
  }

  @Override
  public void delete(String id) {
    Client storedClient = findById(id);
    if (storedClient != null) {
      storedClient.setMainClient(null);
      clientRepository.save(storedClient);
    }
    clientRepository.delete(id);

    if (!StringUtils.isBlank(id)) {
      SnsMessageDto msgDto = new SnsMessageDto();
      msgDto.setId(id);
      try {
        messageService.sendMessage(objectMapper.writeValueAsString(msgDto), snsArnDeleteClient,
            SnsMessageOperation.DELETE);
      } catch (Exception e) {
        throw new MessageServiceException("Error sensing message to AWS SNS on Delete Operation "
            + "for Client with ID: " + msgDto.getId());
      }
    }

  }
}
