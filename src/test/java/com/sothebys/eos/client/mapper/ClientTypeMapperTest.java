package com.sothebys.eos.client.mapper;

import static org.junit.Assert.assertEquals;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.model.ClientType;
import com.sothebys.eos.client.repository.ClientTypeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ClientTypeMapperTest {

  @InjectMocks
  private ClientTypeMapper clientTypeMapper = Mappers.getMapper(ClientTypeMapper.class);

  @Mock
  private ClientTypeRepository clientTypeRepository;

  private ClientType clientType;

  @Before
  public void setUp() {
    clientType = DummyModelHelper.createClientTypeModel();
  }

  @Test
  public void mapToModel() {
    Mockito.when(clientTypeRepository.findByName("test")).thenReturn(clientType);

    ClientType response = clientTypeMapper.mapToModel("test");

    assertEquals(new Integer(123), response.getEntityId());
    assertEquals("Individual", response.getName());
  }
}