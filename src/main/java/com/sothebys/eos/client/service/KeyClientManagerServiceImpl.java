package com.sothebys.eos.client.service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.exception.KeyClientManagerNotFoundException;
import com.sothebys.eos.client.model.KeyClientManager;
import com.sothebys.eos.client.model.QKeyClientManager;
import com.sothebys.eos.client.repository.KeyClientManagerRepository;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class KeyClientManagerServiceImpl implements KeyClientManagerService {

  private final KeyClientManagerRepository keyClientManagerRepository;
  private final QKeyClientManager keyClientManagerQuery = QKeyClientManager.keyClientManager;


  /**
   * Constructor.
   *
   * @param keyClientManagerRepository instance.
   */
  @Autowired
  public KeyClientManagerServiceImpl(
      KeyClientManagerRepository keyClientManagerRepository) {
    this.keyClientManagerRepository = keyClientManagerRepository;
  }

  @Override
  public KeyClientManager findByEntityId(String id) {
    if (id == null) {
      throw new KeyClientManagerNotFoundException(id);
    }

    final KeyClientManager storedClient = keyClientManagerRepository
        .findOne(keyClientManagerQuery.filterEntityById(id));

    if (storedClient == null) {
      throw new KeyClientManagerNotFoundException(id);
    }

    return storedClient;
  }

  @Override
  public Page<KeyClientManager> findAll(Predicate predicate, Pageable pageable) {
    Predicate augmentedPredicate = new BooleanBuilder(predicate).getValue();
    return keyClientManagerRepository.findAll(augmentedPredicate, pageable);
  }

  @Override
  @Transactional
  public KeyClientManager create(KeyClientManager client) {

    final KeyClientManager createdKeyClientManager;

    if (StringUtils.isBlank(client.getEntityId())) {
      createdKeyClientManager = keyClientManagerRepository
          .insertWithIdGeneration(client);
    } else {
      createdKeyClientManager = keyClientManagerRepository.save(client);
    }

    return createdKeyClientManager;
  }

  @Override
  @Transactional
  public KeyClientManager update(KeyClientManager client) {

    final KeyClientManager updatedClient = keyClientManagerRepository.save(client);

    return findByEntityId(updatedClient.getEntityId());
  }

  @Override
  @Transactional
  public void delete(String id) {
    keyClientManagerRepository.delete(id);
  }
}
