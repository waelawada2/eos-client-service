package com.sothebys.eos.client.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sothebys.eos.client.validation.constraint.UniqueValue;
import com.sothebys.eos.client.validation.constraint.UniqueValues;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import java.time.Instant;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(exclude = "mainClient", callSuper = true)
@ToString(exclude = "mainClient")
@Entity
@UniqueValues({
    @UniqueValue(fieldName = "entityId", dbAlias = "id",
        groups = {CreateValidation.class})
})
public class Client extends EntityWithSequenceGeneratedId implements EntityBase {

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @Column(nullable = false)
  private String name;

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @ManyToOne(optional = false)
  @JoinColumn(nullable = false)
  private ClientType clientType;

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @ManyToOne(optional = false)
  @JoinColumn(nullable = false)
  private ClientStatus clientStatus;

  @ManyToOne
  private KeyClientManager keyClientManager;

  @ManyToOne
  private Client mainClient;

  private String city;

  private String state;

  private String country;

  private String region;

  @ManyToOne
  private MarketingSegment marketingSegment;

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @Min(value = 0, groups = {CreateValidation.class, UpdateValidation.class})
  @Max(value = 10, groups = {CreateValidation.class, UpdateValidation.class})
  @Column(nullable = false)
  private Integer level;

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @Column(updatable = false)
  private Boolean fromClientSystem;

  @OneToMany(mappedBy = "client")
  private List<ClientAssociation> clientAssociations;

  @Embedded
  private Individual individual;

  @JsonIgnore
  private Instant lastSyncDate;

  @Override
  public String getSequenceName() {
    return "clients_seq";
  }
}
