package com.sothebys.eos.client.service;

import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.model.ClientAssociation;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

@Validated
public interface ClientAssociationService {

  @Validated(UpdateValidation.class)
  ClientAssociation save(@Valid ClientAssociation clientAssociation);

  void deleteByClientSystemPartyAssociationId(Long partyAssociationId);

  ClientAssociation findById(Long id);

  @Validated(CreateValidation.class)
  ClientAssociation create(@Valid ClientAssociation clientAssociation);

  @Validated(UpdateValidation.class)
  ClientAssociation update(@Valid ClientAssociation clientAssociation);

  Page<ClientAssociation> findAll(Predicate predicate, Pageable pageable);

  void delete(final Long id);
}
