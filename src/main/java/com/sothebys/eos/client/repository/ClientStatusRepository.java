package com.sothebys.eos.client.repository;

import com.sothebys.eos.client.model.ClientStatus;
import com.sothebys.eos.client.model.QClientStatus;
import com.sothebys.eos.client.repository.support.SimpleCrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "client-statuses")
public interface ClientStatusRepository extends SimpleCrudRepository<ClientStatus, Integer,
    QClientStatus> {

  ClientStatus findByName(String name);
}
