package com.sothebys.eos.client.exception;

@SuppressWarnings("serial")
public class ElementNotFoundException extends ClientServiceException {

  protected final String nonexistentId;

  /**
   * Base class for Not found errors.
   *
   * @param id The nonexistent id
   * @param message The message
   */
  public ElementNotFoundException(String id, String message) {
    super(message);

    this.nonexistentId = id;
  }

  public String getNonexistentId() {
    return nonexistentId;
  }

}
