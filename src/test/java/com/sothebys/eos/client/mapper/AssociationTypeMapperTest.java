package com.sothebys.eos.client.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.model.AssociationType;
import com.sothebys.eos.client.repository.AssociationTypeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class AssociationTypeMapperTest {

  @InjectMocks
  private AssociationTypeMapper associationTypeMapper = Mappers
      .getMapper(AssociationTypeMapper.class);

  @Mock
  private AssociationTypeRepository associationTypeRepository;

  private AssociationType associationType;

  @Before
  public void setUp() {
    associationType = DummyModelHelper.createAssociationTypeModel();
  }

  @Test
  public void mapToModelFromClientSystemIdMemberOf() {
    Mockito.when(associationTypeRepository.findByName("member of")).thenReturn(associationType);

    AssociationType response = associationTypeMapper.mapToModelFromClientSystemId(42);

    assertEquals(new Integer(123), response.getEntityId());
    assertEquals("member of", response.getName());
  }

  @Test
  public void mapToModelFromClientSystemIdContactFor() {
    Mockito.when(associationTypeRepository.findByName("contact for")).thenReturn(associationType);

    AssociationType response = associationTypeMapper.mapToModelFromClientSystemId(112);

    assertEquals(new Integer(123), response.getEntityId());
    assertEquals("member of", response.getName());
  }

  @Test
  public void mapToModelFromClientSystemIdCompanyDirectorOf() {
    Mockito.when(associationTypeRepository.findByName("company director of")).thenReturn(associationType);

    AssociationType response = associationTypeMapper.mapToModelFromClientSystemId(113);

    assertEquals(new Integer(123), response.getEntityId());
    assertEquals("member of", response.getName());
  }

  @Test
  public void mapToModelFromClientSystemIdDeceasedMemberOf() {
    Mockito.when(associationTypeRepository.findByName("deceased member of")).thenReturn(associationType);

    AssociationType response = associationTypeMapper.mapToModelFromClientSystemId(122);

    assertEquals(new Integer(123), response.getEntityId());
    assertEquals("member of", response.getName());
  }

  @Test
  public void mapToModelFromClientSystemIdUnhandledId() {
    Mockito.when(associationTypeRepository.findByName(null)).thenReturn(null);
    AssociationType response = associationTypeMapper.mapToModelFromClientSystemId(999);
    assertNull(response);
  }

  @Test
  public void mapToModelFromClientSystemIdNull() {
    AssociationType response = associationTypeMapper.mapToModelFromClientSystemId(null);
    assertNull(response);
  }
}