package com.sothebys.eos.client.dto;

import java.time.Instant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@Data
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "clientAssociations", value = "clientAssociation")
public class ClientAssociationDto extends ResourceSupport {

  private Long entityId;

  private ClientDto client;

  private ClientDto associatedClient;

  private AssociationTypeDto associationType;

  private Long clientSystemPartyAssociationId;

  private Instant lastSyncDate;
}
