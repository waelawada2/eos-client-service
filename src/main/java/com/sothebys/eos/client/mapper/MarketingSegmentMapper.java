package com.sothebys.eos.client.mapper;

import com.sothebys.eos.client.model.MarketingSegment;
import com.sothebys.eos.client.repository.MarketingSegmentRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class MarketingSegmentMapper {

  @Autowired
  private MarketingSegmentRepository marketingSegmentRepository;

  MarketingSegment mapToModel(String name) {
    return marketingSegmentRepository.findByName(name);
  }
}
