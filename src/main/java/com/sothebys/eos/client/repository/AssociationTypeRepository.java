package com.sothebys.eos.client.repository;

import com.sothebys.eos.client.model.AssociationType;
import com.sothebys.eos.client.model.QAssociationType;
import com.sothebys.eos.client.repository.support.SimpleCrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "association-types")
public interface AssociationTypeRepository extends SimpleCrudRepository<AssociationType, Integer,
    QAssociationType> {

  AssociationType findByName(String name);
}
