package com.sothebys.eos.client.model;

import com.sothebys.eos.client.validation.constraint.UniqueValue;
import com.sothebys.eos.client.validation.constraint.UniqueValues;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@UniqueValues({
    @UniqueValue(fieldName = "entityId", dbAlias = "id",
        groups = {CreateValidation.class})
})
public class KeyClientManager extends EntityWithSequenceGeneratedId implements EntityBase {

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @Column(nullable = false)
  private String name;


  @Override
  public String getSequenceName() {
    return "key_client_manager_seq";
  }
}