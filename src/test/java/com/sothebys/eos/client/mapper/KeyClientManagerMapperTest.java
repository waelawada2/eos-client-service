package com.sothebys.eos.client.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.model.KeyClientManager;
import com.sothebys.eos.client.repository.KeyClientManagerRepository;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class KeyClientManagerMapperTest {

  @InjectMocks
  private KeyClientManagerMapper keyClientManagerMapper = Mappers
      .getMapper(KeyClientManagerMapper.class);

  @Mock
  private KeyClientManagerRepository keyClientManagerRepository;

  private KeyClientManager keyClientManager;

  @Before
  public void setUp() {
    keyClientManager = DummyModelHelper.createKeyClientManagerModel();
  }

  @Test
  public void mapToModel() {
    Mockito.when(keyClientManagerRepository.findByEntityId("12345"))
        .thenReturn(Optional.of(keyClientManager));

    KeyClientManager response = keyClientManagerMapper.mapToModel("12345");

    assertEquals("12345", response.getEntityId());
    assertEquals("Bob Dylan", response.getName());
  }

  @Test
  public void mapToModelNull() {
    Mockito.when(keyClientManagerRepository.findByEntityId("12345"))
        .thenReturn(Optional.empty());
    KeyClientManager response = keyClientManagerMapper.mapToModel("12345");
    assertNull(response);
  }
}