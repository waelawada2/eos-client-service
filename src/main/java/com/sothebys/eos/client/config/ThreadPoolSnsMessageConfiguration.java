package com.sothebys.eos.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadPoolSnsMessageConfiguration {

  @Value("${threadpool.sns-message.corepoolsize:10}")
  int corePoolSize;

  @Value("${threadpool.sns-message.maxpoolsize:25}")
  int maxPoolSize;

  /**
   * Thread Pool Task Executor to manage asyncronous methods.
   * @return ThreadPoolTaskExecutor.
   */
  @Bean
  public ThreadPoolTaskExecutor snsMessageTaskExecutor() {
    ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
    pool.setCorePoolSize(corePoolSize);
    pool.setMaxPoolSize(maxPoolSize);
    pool.setWaitForTasksToCompleteOnShutdown(true);
    return pool;
  }
}
