package com.sothebys.eos.client.exception;

public class ClientNotFoundException extends ElementNotFoundException {

  /**
   * Base class for Not found errors.
   *
   * @param id The inexistent id
   */
  public ClientNotFoundException(String id) {
    super(id, String.format("Client with id '%s' was not found.", id));
  }
}
