package com.sothebys.eos.client.dto.clientsystem;

import java.time.Instant;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ClientAssociationUpsertDto {

  @NotNull
  private Long partyAssociationId;

  @NotNull
  private Long fromPartyId;

  @NotNull
  private Long toPartyId;

  @NotNull
  private Integer associationTypeId;

  private Instant lastSyncDate;
}
