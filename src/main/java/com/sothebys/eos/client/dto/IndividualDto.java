package com.sothebys.eos.client.dto;

import java.time.LocalDate;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;

@Data
@EqualsAndHashCode(callSuper = false)
public class IndividualDto extends ResourceSupport {

  private String title;

  private String firstName;

  private String middleName;

  private String lastName;

  private GenderDto gender;

  private LocalDate birthDate;

  private String suffix;

  private boolean deceased;

}
