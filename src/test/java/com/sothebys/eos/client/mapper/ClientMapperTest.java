package com.sothebys.eos.client.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.exception.ClientNotFoundException;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.service.ClientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ClientMapperTest {

  @InjectMocks
  private ClientMapper clientMapper = Mappers.getMapper(ClientMapper.class);

  @Mock
  private ClientService clientService;

  private Client client;

  @Before
  public void setUp() {
    client = DummyModelHelper.createClientModel();
  }

  @Test
  public void mapToModelFromExternalId() {
    Mockito.when(clientService.findById("12345")).thenReturn(client);

    Client response = clientMapper.mapToModel(12345L);


    assertEquals("12345", response.getEntityId());
    assertEquals("John Smith", response.getName());
  }

  @Test(expected = ClientNotFoundException.class)
  public void mapToModelFromExternalIdNull() {
    Mockito.when(clientService.findById(null)).thenThrow(new ClientNotFoundException(""));
    clientMapper.mapToModel((Long)null);
  }

  @Test
  public void mapToModelNullIfNotFound() {
    Mockito.when(clientService.findById("12345")).thenReturn(client);

    Client response = clientMapper.mapToModelNullIfNotFound(12345L);


    assertEquals("12345", response.getEntityId());
    assertEquals("John Smith", response.getName());
  }

  @Test
  public void mapToModelNullIfNotFoundNull() {
    Mockito.when(clientService.findById(null)).thenThrow(new ClientNotFoundException(""));
    Client response = clientMapper.mapToModelNullIfNotFound(null);
    assertNull(response);
  }

  @Test
  public void mapLevelStringToInt() {
    int response = clientMapper.mapLevelStringToInt("Level 9");
    assertEquals(9, response);
  }
}