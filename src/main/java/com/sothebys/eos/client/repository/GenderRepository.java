package com.sothebys.eos.client.repository;

import com.sothebys.eos.client.model.Gender;
import com.sothebys.eos.client.model.QGender;
import com.sothebys.eos.client.repository.support.SimpleCrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface GenderRepository extends SimpleCrudRepository<Gender, Integer, QGender> {

  Gender findByName(String name);
}
