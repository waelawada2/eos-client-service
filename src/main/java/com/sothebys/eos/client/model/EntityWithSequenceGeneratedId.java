package com.sothebys.eos.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.Data;

@Data
@MappedSuperclass
public abstract class EntityWithSequenceGeneratedId {

  @Id
  @JsonProperty("id")
  @Column(name = "id", unique = true, updatable = false, nullable = false)
  protected String entityId;

  public abstract String getSequenceName();
}
