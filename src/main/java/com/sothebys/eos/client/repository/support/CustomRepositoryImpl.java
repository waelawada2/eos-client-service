package com.sothebys.eos.client.repository.support;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.sothebys.eos.client.model.EntityWithSequenceGeneratedId;
import com.sothebys.eos.client.service.IdGeneratorService;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;


@Slf4j
public class CustomRepositoryImpl<T extends EntityWithSequenceGeneratedId, I extends Serializable>
    extends QueryDslJpaRepository<T, I> implements CustomRepository<T> {

  //All instance variables are available in super, but they are private
  private static final EntityPathResolver DEFAULT_ENTITY_PATH_RESOLVER =
      SimpleEntityPathResolver.INSTANCE;

  private final EntityPath<T> path;
  private final PathBuilder<T> builder;
  private final Querydsl querydsl;

  private IdGeneratorService idGeneratorService;

  public CustomRepositoryImpl(
      JpaEntityInformation<T, I> entityInformation,
      EntityManager entityManager) {
    this(entityInformation, entityManager, DEFAULT_ENTITY_PATH_RESOLVER);
  }

  /**
   * Constructor.
   * @param entityInformation instance.
   * @param entityManager instance.
   * @param resolver instance.
   */
  public CustomRepositoryImpl(JpaEntityInformation<T, I> entityInformation,
      EntityManager entityManager,
      EntityPathResolver resolver) {

    super(entityInformation, entityManager);
    this.path = resolver.createPath(entityInformation.getJavaType());
    this.builder = new PathBuilder<T>(path.getType(), path.getMetadata());
    this.querydsl = new Querydsl(entityManager, builder);
  }


  public void setIdGeneratorService(IdGeneratorService idGeneratorService) {
    this.idGeneratorService = idGeneratorService;
  }

  /**
   * Generate an id using {@link IdGeneratorService} and then insert the entity.  If the insert
   * fails due to {@link DuplicateKeyException}, then this function retries until it succeeds.
   *
   * @param entity entity to be inserted
   * @param <S> S
   * @return the inserted entity
   */
  public <S extends T> S insertWithIdGeneration(S entity) {
    while (true) {
      entity.setEntityId(idGeneratorService.generateId(entity.getSequenceName()));
      try {
        return super.save(entity);
      } catch (DuplicateKeyException e) {
        log.warn("Duplicate key used while trying to insert {}. Retrying",
            entity.getClass().getSimpleName());
      }
    }
  }

  @Override
  public Page<T> findAll(FactoryExpression<T> factoryExpression, Predicate predicate,
      Pageable pageable) {
    JPQLQuery countQuery = createQuery(predicate);
    JPQLQuery query = querydsl.applyPagination(pageable, createQuery(predicate));

    query = query.select(factoryExpression);

    Long total = countQuery.fetchCount();
    List<T> content =
        total > pageable.getOffset() ? query.fetch() : Collections.<T>emptyList();

    return new PageImpl<T>(content, pageable, total);
  }
}
