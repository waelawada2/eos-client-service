package com.sothebys.eos.client.mapper;

import static org.junit.Assert.assertEquals;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.model.ClientStatus;
import com.sothebys.eos.client.repository.ClientStatusRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ClientStatusMapperTest {

  @InjectMocks
  private ClientStatusMapper clientStatusMapper = Mappers.getMapper(ClientStatusMapper.class);

  @Mock
  private ClientStatusRepository clientStatusRepository;

  private ClientStatus clientStatus;

  @Before
  public void setUp() {
    clientStatus = DummyModelHelper.createClientStatusModel();
  }

  @Test
  public void mapToModel() {
    Mockito.when(clientStatusRepository.findByName("test")).thenReturn(clientStatus);

    ClientStatus response = clientStatusMapper.mapToModel("test");

    assertEquals(new Integer(123), response.getEntityId());
    assertEquals("Active", response.getName());
  }
}