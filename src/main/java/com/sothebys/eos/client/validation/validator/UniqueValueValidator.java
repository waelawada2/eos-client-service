package com.sothebys.eos.client.validation.validator;

import com.sothebys.eos.client.model.EntityBase;
import com.sothebys.eos.client.validation.constraint.UniqueValue;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;


public class UniqueValueValidator implements BaseValidator<UniqueValue, Object> {

  private String fieldName;

  private EntityManager entityManager;

  @Autowired
  public UniqueValueValidator(EntityManager entityManager) {
    this.entityManager = entityManager;
  }


  @Override
  public void initialize(UniqueValue constraintAnnotation) {
    this.fieldName = constraintAnnotation.fieldName();

  }

  @Override
  public boolean isValid(Object value, ConstraintValidatorContext context) {


    Class modelClass = value.getClass();

    try {
      String fieldValue = BeanUtils.getProperty(value, fieldName);

      CriteriaBuilder cb = entityManager.getCriteriaBuilder();
      CriteriaQuery query = cb.createQuery(modelClass);
      Root root = query.from(modelClass);
      query.select(root).where(cb.equal(root.<String>get(fieldName), fieldValue));

      TypedQuery<? extends EntityBase> entitiesQuery = entityManager.createQuery(query);
      final List<? extends EntityBase> entities = entitiesQuery.getResultList();

      if (entities.size() > 0) {
        addErrorMessage(context, fieldName, context.getDefaultConstraintMessageTemplate(),
            modelClass.getSimpleName(), fieldName, fieldValue);
        return false;
      }

    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
      throw new RuntimeException(ex);
    }
    return true;
  }


}
