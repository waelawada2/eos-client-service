package com.sothebys.eos.client.model;

import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
public class Title implements EntityBase {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private Integer entityId;

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @Column(nullable = false)
  private String name;

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @ManyToOne(optional = false)
  @JoinColumn(nullable = false)
  private Gender gender;
}
