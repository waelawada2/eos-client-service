package com.sothebys.eos.client.repository;

import com.sothebys.eos.client.model.ClientAssociation;
import com.sothebys.eos.client.model.QClientAssociation;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientAssociationRepository extends
    PagingAndSortingRepository<ClientAssociation, Long>,
    JpaRepository<ClientAssociation, Long>,
    QueryDslPredicateExecutor<ClientAssociation>, QuerydslBinderCustomizer<QClientAssociation> {

  default void customize(QuerydslBindings bindings, QClientAssociation clientAssociation) {

  }


  Optional<ClientAssociation> findByClientSystemPartyAssociationId(
      Long clientSystemPartyAssociationId);

  void deleteByClientSystemPartyAssociationId(Long partyAssociationId);
}
