package com.sothebys.eos.client.sqs.listener;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.dto.clientsystem.ClientAssociationUpsertDto;
import com.sothebys.eos.client.dto.clientsystem.ClientUpsertDto;
import com.sothebys.eos.client.dto.clientsystem.MessageDto;
import com.sothebys.eos.client.exception.InvalidSqsMessageException;
import com.sothebys.eos.client.sqs.support.ClientAssociationHandler;
import com.sothebys.eos.client.sqs.support.ClientHandler;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClientSystemSqsListenerTest {

  @InjectMocks
  private ClientSystemSqsListener clientSystemSqsListener;

  @Mock
  private ObjectMapper objectMapper;

  @Mock
  private ClientHandler clientHandler;

  @Mock
  private ClientAssociationHandler clientAssociationHandler;

  private ObjectMapper realObjectMapper = new ObjectMapper();
  private Map<String, String> headers;
  private Map<String, String> headersWithoutMessage;
  private Map<String, String> headersWithoutTimestamp;
  private MessageDto createClientMessageDto;
  private MessageDto updateClientMessageDto;
  private MessageDto updateClientAssociationMessageDto;
  private MessageDto deleteClientAssociationMessageDto;
  private MessageDto deleteClientAssociationMessageDtoInvalidPayload;
  private MessageDto messageDtoNullHeader;
  private MessageDto messageDtoNullHeaderString;
  private MessageDto messageDtoIgnored;
  private ClientUpsertDto clientUpsertDto;
  private ClientAssociationUpsertDto clientAssociationUpsertDto;

  @Before
  public void setUp() throws Exception {
    headers = createHeaders();

    headersWithoutMessage = createHeaders();
    headersWithoutMessage.remove("Message");

    headersWithoutTimestamp = createHeaders();
    headersWithoutTimestamp.remove("Timestamp");

    createClientMessageDto = createMessageDto();
    createClientMessageDto.getHeader().setHeader("CREATED_CLIENT");

    updateClientMessageDto = createMessageDto();
    updateClientMessageDto.getHeader().setHeader("UPDATED_CLIENT");

    updateClientAssociationMessageDto = createMessageDto();
    updateClientAssociationMessageDto.getHeader().setHeader("UPDATED_CLIENT_ASSOCIATION");

    deleteClientAssociationMessageDto = createMessageDto();
    deleteClientAssociationMessageDto.getHeader().setHeader("DELETED_CLIENT_ASSOCIATION");
    ((ObjectNode) deleteClientAssociationMessageDto.getPayload()).put("partyAssociationId", 123L);

    deleteClientAssociationMessageDtoInvalidPayload = createMessageDto();
    deleteClientAssociationMessageDto.getHeader().setHeader("DELETED_CLIENT_ASSOCIATION");

    messageDtoNullHeader = createMessageDto();
    messageDtoNullHeader.setHeader(null);

    messageDtoNullHeaderString = createMessageDto();
    messageDtoNullHeaderString.getHeader().setHeader(null);

    messageDtoIgnored = createMessageDto();
    messageDtoIgnored.getHeader().setHeader("SOME_VALUE");

    clientUpsertDto = DummyModelHelper.createClientUpsertDto();
    clientAssociationUpsertDto = DummyModelHelper.createClientAssociationUpsertDto();
  }

  private Map<String, String> createHeaders() {
    Map<String, String> headers = new HashMap<>();
    headers.put("Message", "some message");
    headers.put("Timestamp", "2012-04-25T21:49:25.719Z");
    return headers;
  }

  private MessageDto createMessageDto() throws Exception {
    JsonNode jsonNode = realObjectMapper.readTree("{\"test\": 123}");

    MessageDto messageDto = new MessageDto();
    messageDto.setHeader(new MessageDto.Header());
    messageDto.setPayload(jsonNode);
    return messageDto;
  }

  @Test
  public void queueListenerCreateClient() throws Exception {
    Mockito.when(objectMapper.readValue("some message", MessageDto.class))
        .thenReturn(createClientMessageDto);
    Mockito
        .when(objectMapper.treeToValue(createClientMessageDto.getPayload(), ClientUpsertDto.class))
        .thenReturn(clientUpsertDto);
    Mockito.doNothing().when(clientHandler).createClient(clientUpsertDto);

    clientSystemSqsListener.queueListener(headers);

    assertEquals("2012-04-25T21:49:25.719Z",
        createClientMessageDto.getPayload().get("lastSyncDate").asText());
    Mockito.verify(clientHandler).createClient(clientUpsertDto);
  }

  @Test
  public void queueListenerUpdateClient() throws Exception {
    Mockito.when(objectMapper.readValue("some message", MessageDto.class))
        .thenReturn(updateClientMessageDto);
    Mockito
        .when(objectMapper.treeToValue(updateClientMessageDto.getPayload(), ClientUpsertDto.class))
        .thenReturn(clientUpsertDto);
    Mockito.doNothing().when(clientHandler).updateClient(clientUpsertDto);

    clientSystemSqsListener.queueListener(headers);

    assertEquals("2012-04-25T21:49:25.719Z",
        updateClientMessageDto.getPayload().get("lastSyncDate").asText());
    Mockito.verify(clientHandler).updateClient(clientUpsertDto);
  }

  @Test
  public void queueListenerUpdateClientAssociation() throws Exception {
    Mockito.when(objectMapper.readValue("some message", MessageDto.class))
        .thenReturn(updateClientAssociationMessageDto);
    Mockito.when(objectMapper.treeToValue(updateClientAssociationMessageDto.getPayload(),
        ClientAssociationUpsertDto.class)).thenReturn(clientAssociationUpsertDto);
    Mockito.doNothing().when(clientAssociationHandler)
        .updateClientAssociation(clientAssociationUpsertDto);

    clientSystemSqsListener.queueListener(headers);

    assertEquals("2012-04-25T21:49:25.719Z",
        updateClientAssociationMessageDto.getPayload().get("lastSyncDate").asText());
    Mockito.verify(clientAssociationHandler).updateClientAssociation(clientAssociationUpsertDto);
  }

  @Test
  public void queueListenerDeleteClientAssociation() throws Exception {
    Mockito.when(objectMapper.readValue("some message", MessageDto.class))
        .thenReturn(deleteClientAssociationMessageDto);
    Mockito.doNothing().when(clientAssociationHandler).deleteClientAssociation(123L);

    clientSystemSqsListener.queueListener(headers);

    Mockito.verify(clientAssociationHandler).deleteClientAssociation(123L);
  }

  @Test(expected = InvalidSqsMessageException.class)
  public void queueListenerDeleteClientAssociationInvalidPayload() throws Exception {
    Mockito.when(objectMapper.readValue("some message", MessageDto.class))
        .thenReturn(deleteClientAssociationMessageDtoInvalidPayload);

    clientSystemSqsListener.queueListener(headers);
  }

  @Test(expected = InvalidSqsMessageException.class)
  public void queueListenerNoMessageHeader() throws Exception {
    clientSystemSqsListener.queueListener(headersWithoutMessage);
  }

  @Test(expected = InvalidSqsMessageException.class)
  public void queueListenerNoTimestampHeader() throws Exception {
    clientSystemSqsListener.queueListener(headersWithoutTimestamp);
  }

  @Test(expected = InvalidSqsMessageException.class)
  public void queueListenerMessageNullHeader() throws Exception {
    Mockito.when(objectMapper.readValue("some message", MessageDto.class))
        .thenReturn(messageDtoNullHeader);
    clientSystemSqsListener.queueListener(headersWithoutMessage);
  }

  @Test(expected = InvalidSqsMessageException.class)
  public void queueListenerMessageNullHeaderString() throws Exception {
    Mockito.when(objectMapper.readValue("some message", MessageDto.class))
        .thenReturn(messageDtoNullHeaderString);
    clientSystemSqsListener.queueListener(headersWithoutMessage);
  }

  @Test(expected = InvalidSqsMessageException.class)
  public void queueListenerIgnoredMessageHeader() throws Exception {
    Mockito.when(objectMapper.readValue("some message", MessageDto.class))
        .thenReturn(messageDtoIgnored);

    clientSystemSqsListener.queueListener(headersWithoutMessage);

    Mockito.verify(clientHandler, Mockito.never()).createClient(clientUpsertDto);
    Mockito.verify(clientHandler, Mockito.never()).updateClient(clientUpsertDto);
    Mockito.verify(clientAssociationHandler, Mockito.never())
        .updateClientAssociation(clientAssociationUpsertDto);
  }
}