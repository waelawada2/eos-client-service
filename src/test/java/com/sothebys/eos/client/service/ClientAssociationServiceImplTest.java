package com.sothebys.eos.client.service;

import static org.junit.Assert.assertEquals;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.exception.ClientServiceException;
import com.sothebys.eos.client.model.ClientAssociation;
import com.sothebys.eos.client.repository.ClientAssociationRepository;
import java.time.Instant;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClientAssociationServiceImplTest {

  @InjectMocks
  private ClientAssociationServiceImpl clientAssociationService;

  @Mock
  private ClientAssociationRepository clientAssociationRepository;

  private ClientAssociation clientAssociation;
  private ClientAssociation secondClientAssociation;
  private ClientAssociation clientAssociationWithBiggerLastSyncDate;

  @Before
  public void setUp() {
    clientAssociation = DummyModelHelper.createClientAssociationModel();
    secondClientAssociation = DummyModelHelper.createClientAssociationModel();
    secondClientAssociation.setEntityId(456L);
    clientAssociationWithBiggerLastSyncDate = DummyModelHelper.createClientAssociationModel();
    clientAssociationWithBiggerLastSyncDate.setLastSyncDate(Instant.ofEpochMilli(1516656684649L));
  }

  @Test
  public void save() {
    Mockito.when(clientAssociationRepository.findByClientSystemPartyAssociationId(646L))
        .thenReturn(Optional.of(secondClientAssociation));
    Mockito.when(clientAssociationRepository.save(clientAssociation)).thenReturn(clientAssociation);

    ClientAssociation response = clientAssociationService.save(clientAssociation);

    assertEquals(new Long(456), response.getEntityId());
    assertEquals("12345", response.getClient().getEntityId());
    assertEquals("456", response.getAssociatedClient().getEntityId());
    assertEquals(new Integer(123), response.getAssociationType().getEntityId());
  }

  @Test
  public void saveNoExistingClientAssociation() {
    Mockito.when(clientAssociationRepository.findByClientSystemPartyAssociationId(646L))
        .thenReturn(Optional.empty());
    Mockito.when(clientAssociationRepository.save(clientAssociation)).thenReturn(clientAssociation);

    ClientAssociation response = clientAssociationService.save(clientAssociation);

    assertEquals(new Long(123), response.getEntityId());
    assertEquals("12345", response.getClient().getEntityId());
    assertEquals("456", response.getAssociatedClient().getEntityId());
    assertEquals(new Integer(123), response.getAssociationType().getEntityId());
  }

  @Test(expected = ClientServiceException.class)
  public void saveRequestDateBeforeLastSyncDate() {
    Mockito.when(clientAssociationRepository.findByClientSystemPartyAssociationId(646L))
        .thenReturn(Optional.ofNullable(clientAssociationWithBiggerLastSyncDate));
    clientAssociationService.save(clientAssociation);
  }

  @Test
  public void deleteByClientSystemPartyAssociationId() {
    Mockito.doNothing().when(clientAssociationRepository).deleteByClientSystemPartyAssociationId(123L);
    clientAssociationService.deleteByClientSystemPartyAssociationId(123L);
    Mockito.verify(clientAssociationRepository).deleteByClientSystemPartyAssociationId(123L);
  }
}