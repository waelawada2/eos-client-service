package com.sothebys.eos.client.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.model.Gender;
import com.sothebys.eos.client.repository.GenderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class GenderMapperTest {

  @InjectMocks
  private GenderMapper genderMapper = Mappers.getMapper(GenderMapper.class);

  @Mock
  private GenderRepository genderRepository;

  private Gender gender;

  @Before
  public void setUp() {
    gender = DummyModelHelper.createGenderModel();
  }

  @Test
  public void mapToModelMale() {
    Mockito.when(genderRepository.findByName("Male")).thenReturn(gender);

    Gender response = genderMapper.mapToModel("M");

    assertEquals(new Integer(123), response.getEntityId());
    assertEquals("Male", response.getName());
  }

  @Test
  public void mapToModelFemale() {
    Mockito.when(genderRepository.findByName("Female")).thenReturn(gender);

    Gender response = genderMapper.mapToModel("F");

    assertEquals(new Integer(123), response.getEntityId());
    assertEquals("Male", response.getName());
  }

  @Test
  public void mapToModelUnhandledCode() {
    Mockito.when(genderRepository.findByName(null)).thenReturn(null);
    Gender response = genderMapper.mapToModel("BLAH");
    assertNull(response);
  }

  @Test
  public void mapToModelNull() {
    Gender response = genderMapper.mapToModel(null);
    assertNull(response);
  }
}