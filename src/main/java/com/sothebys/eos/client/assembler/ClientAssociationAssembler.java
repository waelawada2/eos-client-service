package com.sothebys.eos.client.assembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import com.sothebys.eos.client.controller.ClientAssociationController;
import com.sothebys.eos.client.controller.ClientController;
import com.sothebys.eos.client.dto.ClientAssociationDto;
import com.sothebys.eos.client.mapper.ClientAssociationMapper;
import com.sothebys.eos.client.model.ClientAssociation;
import com.sothebys.eos.client.repository.AssociationTypeRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ClientAssociationAssembler extends
    ResourceAssemblerSupport<ClientAssociation, ClientAssociationDto> {

  private final ClientAssociationMapper mapper;
  private final RepositoryEntityLinks repositoryEntityLinks;

  @Autowired
  private ClientAssociationAssembler(ClientAssociationMapper mapper,
      RepositoryEntityLinks repositoryEntityLinks) {
    super(ClientAssociationController.class, ClientAssociationDto.class);
    this.repositoryEntityLinks = repositoryEntityLinks;

    this.mapper = mapper;
  }

  @Override
  public List<ClientAssociationDto> toResources(Iterable<? extends ClientAssociation> entities) {
    final List<ClientAssociationDto> resources = new ArrayList<>();

    entities.forEach(entity -> resources.add(toResource(entity)));

    return resources;
  }

  @Override
  public ClientAssociationDto toResource(ClientAssociation entity) {
    ClientAssociationDto dto = mapper.mapToDto(entity);

    dto.add(linkTo(ClientAssociationController.class).slash(dto.getEntityId()).withSelfRel());

    dto.add(linkTo(ClientAssociationController.class).slash(dto.getEntityId())
        .withRel("clientAssociation"));

    if (dto.getAssociationType() != null) {
      dto.add(createLink(AssociationTypeRepository.class, dto.getAssociationType()
          .getEntityId().toString()).withRel("associationType"));
    }

    if (dto.getClient() != null) {
      dto.add(
          linkTo(ClientController.class).slash(dto.getClient().getEntityId()).withRel("client"));
    }

    if (dto.getAssociatedClient() != null) {
      dto.add(linkTo(ClientController.class).slash(dto.getAssociatedClient().getEntityId())
          .withRel("associatedClient"));
    }

    return dto;
  }


  private Link createLink(Class<?> clazz, String id) {
    return repositoryEntityLinks.linkToSingleResource(clazz, id);
  }
}
