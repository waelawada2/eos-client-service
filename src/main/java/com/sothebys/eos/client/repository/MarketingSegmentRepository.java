package com.sothebys.eos.client.repository;

import com.sothebys.eos.client.model.MarketingSegment;
import com.sothebys.eos.client.model.QMarketingSegment;
import com.sothebys.eos.client.repository.support.SimpleCrudRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "marketing-segments")
public interface MarketingSegmentRepository extends
    SimpleCrudRepository<MarketingSegment, Integer, QMarketingSegment> {

  MarketingSegment findByName(String name);
}
