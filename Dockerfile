FROM 368978185270.dkr.ecr.us-east-1.amazonaws.com/stb-oraclejdk-overops:latest

COPY target/eos-client-service*.jar app.jar
COPY docker/app.sh /
RUN chmod +x /app.sh

# Install missing dependency for MariaDB
RUN apk add --no-cache ncurses5-libs

EXPOSE 9970

ENV PORT=9970
ENV JAVA_OPTS="-Xmx512M -Djava.security.egd=file:/dev/./urandom"

ENTRYPOINT [ "/app.sh" ]
