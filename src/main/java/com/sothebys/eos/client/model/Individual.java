package com.sothebys.eos.client.model;

import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Data
@Embeddable
public class Individual {

  private String title;

  @Size(max = 35, groups = {CreateValidation.class, UpdateValidation.class})
  @Column(length = 35)
  private String firstName;

  private String middleName;

  @Size(max = 35, groups = {CreateValidation.class, UpdateValidation.class})
  @Column(length = 35)
  private String lastName;

  private String suffix;

  @ManyToOne
  private Gender gender;

  @DateTimeFormat(iso = ISO.DATE)
  private LocalDate birthDate;

  private boolean deceased;
}
