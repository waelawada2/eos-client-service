package com.sothebys.eos.client.mapper;

import static org.junit.Assert.assertEquals;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.model.MarketingSegment;
import com.sothebys.eos.client.repository.MarketingSegmentRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class MarketingSegmentMapperTest {

  @InjectMocks
  private MarketingSegmentMapper marketingSegmentMapper = Mappers.getMapper(MarketingSegmentMapper.class);

  @Mock
  private MarketingSegmentRepository marketingSegmentRepository;

  private MarketingSegment marketingSegment;

  @Before
  public void setUp() {
    marketingSegment = DummyModelHelper.createMarketingSegmentModel();
  }

  @Test
  public void mapToModel() {
    Mockito.when(marketingSegmentRepository.findByName("test")).thenReturn(marketingSegment);

    MarketingSegment response = marketingSegmentMapper.mapToModel("test");

    assertEquals(new Integer(123), response.getEntityId());
    assertEquals("Private Collector", response.getName());
  }
}