package com.sothebys.eos.client.service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.exception.ClientNotFoundException;
import com.sothebys.eos.client.exception.TitleNotFoundException;
import com.sothebys.eos.client.model.QTitle;
import com.sothebys.eos.client.model.Title;
import com.sothebys.eos.client.repository.TitleRepository;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TitleServiceImpl implements TitleService {

  private final TitleRepository titleRepository;
  private final QTitle titleQuery = QTitle.title;


  /**
   * Constructor.
   *
   * @param titleRepository instance.
   */
  @Autowired
  public TitleServiceImpl(
      TitleRepository titleRepository) {
    this.titleRepository = titleRepository;
  }

  @Override
  public Title findByEntityId(Integer id) {
    if (id == null) {
      throw new ClientNotFoundException("<null>");
    }

    final Title storedClient = titleRepository
        .findOne(new BooleanBuilder(titleQuery.entityId.eq(id)));

    if (storedClient == null) {
      throw new TitleNotFoundException(id);
    }

    return storedClient;
  }

  @Override
  public Page<Title> findAll(Predicate predicate, Pageable pageable) {
    Predicate augmentedPredicate = new BooleanBuilder(predicate).getValue();
    return titleRepository.findAll(augmentedPredicate, pageable);
  }

  @Override
  @Transactional
  public Title create(Title client) {

    final Title createdTitle = titleRepository.save(client);

    return createdTitle;
  }

  @Override
  @Transactional
  public Title update(Title client) {

    final Title updatedClient = titleRepository.save(client);

    return findByEntityId(updatedClient.getEntityId());
  }

  @Override
  @Transactional
  public void delete(Integer id) {
    titleRepository.delete(id);
  }
}
