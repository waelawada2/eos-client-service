package com.sothebys.eos.client.service;

import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.model.Title;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

@Validated
public interface TitleService {

  Title findByEntityId(Integer id);

  Page<Title> findAll(Predicate predicate, Pageable pageable);

  @Validated(CreateValidation.class)
  Title create(@Valid Title client);

  @Validated(UpdateValidation.class)
  Title update(@Valid Title client);

  void delete(final Integer id);
}
