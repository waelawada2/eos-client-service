package com.sothebys.eos.client.exception;

public class InvalidSqsMessageException extends ClientServiceException {

  public InvalidSqsMessageException(String message) {
    super(message);
  }

  public InvalidSqsMessageException(String message, Throwable cause) {
    super(message, cause);
  }
}
