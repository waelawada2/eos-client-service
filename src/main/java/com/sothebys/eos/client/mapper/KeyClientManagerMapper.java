package com.sothebys.eos.client.mapper;

import com.sothebys.eos.client.dto.KeyClientManagerDto;
import com.sothebys.eos.client.model.KeyClientManager;
import com.sothebys.eos.client.repository.KeyClientManagerRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class KeyClientManagerMapper {

  @Autowired
  private KeyClientManagerRepository keyClientManagerRepository;

  public abstract KeyClientManager mapToModel(KeyClientManagerDto dto);

  KeyClientManager mapToModel(String id) {
    return keyClientManagerRepository.findByEntityId(id).orElse(null);
  }

  public abstract KeyClientManagerDto mapToDto(KeyClientManager model);

}
