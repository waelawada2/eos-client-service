package com.sothebys.eos.client.model.validation;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class FieldMessage {

  private String field;
  private String message;

}