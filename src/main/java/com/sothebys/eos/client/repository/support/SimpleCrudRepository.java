package com.sothebys.eos.client.repository.support;

import com.querydsl.core.types.EntityPath;
import com.sothebys.eos.client.model.EntityBase;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface SimpleCrudRepository<T extends EntityBase, I extends Serializable,
    Q extends EntityPath<T>>
    extends PagingAndSortingRepository<T, I>, JpaRepository<T, I>, QueryDslPredicateExecutor<T>,
    QuerydslBinderCustomizer<Q> {

  default void customize(QuerydslBindings bindings, Q entity) {
  }
}
