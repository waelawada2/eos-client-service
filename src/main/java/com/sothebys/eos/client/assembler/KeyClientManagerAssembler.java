package com.sothebys.eos.client.assembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import com.sothebys.eos.client.controller.KeyClientManagerController;
import com.sothebys.eos.client.dto.KeyClientManagerDto;
import com.sothebys.eos.client.mapper.KeyClientManagerMapper;
import com.sothebys.eos.client.model.KeyClientManager;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class KeyClientManagerAssembler extends ResourceAssemblerSupport<KeyClientManager,
    KeyClientManagerDto> {

  private final KeyClientManagerMapper mapper;
  private final RepositoryEntityLinks repositoryEntityLinks;

  @Autowired
  private KeyClientManagerAssembler(KeyClientManagerMapper mapper,
      RepositoryEntityLinks repositoryEntityLinks) {
    super(KeyClientManagerController.class, KeyClientManagerDto.class);
    this.repositoryEntityLinks = repositoryEntityLinks;

    this.mapper = mapper;
  }

  @Override
  public List<KeyClientManagerDto> toResources(Iterable<? extends KeyClientManager> entities) {
    final List<KeyClientManagerDto> resources = new ArrayList<>();

    entities.forEach(entity -> resources.add(toResource(entity)));

    return resources;
  }

  @Override
  public KeyClientManagerDto toResource(KeyClientManager entity) {
    KeyClientManagerDto dto = mapper.mapToDto(entity);

    dto.add(linkTo(KeyClientManagerController.class).slash(dto.getEntityId()).withSelfRel());

    dto.add(linkTo(KeyClientManagerController.class).slash(dto.getEntityId())
        .withRel("keyClientManager"));

    return dto;
  }


  private Link createLink(Class<?> clazz, String id) {
    return repositoryEntityLinks.linkToSingleResource(clazz, id);
  }
}
