package com.sothebys.eos.client.service;

import java.math.BigInteger;
import javax.persistence.EntityManager;
import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IdGeneratorServiceImpl implements IdGeneratorService {

  private static final String PREFIX = "EOS-";
  private final Hashids hashids;
  private EntityManager em;


  /**
   * Constructor.
   *
   * @param hashids Hashids instance.
   * @param em EntityManager instance.
   */
  @Autowired
  public IdGeneratorServiceImpl(Hashids hashids, EntityManager em) {
    this.hashids = hashids;
    this.em = em;

  }

  @Override
  public String generateId(String sequenceName) {

    BigInteger nextVal = (BigInteger) em.createNativeQuery("SELECT nextval('" + sequenceName + "')")
        .setMaxResults(1)
        .getSingleResult();

    final String hashId = hashids.encode(nextVal.longValue());

    return PREFIX + hashId;
  }


}
