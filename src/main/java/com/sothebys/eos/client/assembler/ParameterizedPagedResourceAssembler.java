package com.sothebys.eos.client.assembler;

import javax.servlet.http.HttpServletRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class ParameterizedPagedResourceAssembler<T> extends PagedResourcesAssembler<T> {

  /**
   * Default CustomPagedResourceAssembler constructor.
   */
  public ParameterizedPagedResourceAssembler() {
    super(new HateoasPageableHandlerMethodArgumentResolver(), null);
  }


  /**
   * Get the base request Link including request parameters.
   *
   * @return Link.
   */
  public Link getBaseLink(HttpServletRequest request) {

    if (request == null) {
      request = ((ServletRequestAttributes) RequestContextHolder
          .getRequestAttributes()).getRequest();
    }

    final StringBuffer urlStringBuffer = request.getRequestURL();

    if (!StringUtils.isEmpty(request.getQueryString())) {
      urlStringBuffer.append("?").append(request.getQueryString());
    }

    final Link link = new Link(urlStringBuffer.toString());

    return link;
  }

  /**
   * Get the base request Link including request parameters.
   *
   * @return Link.
   */
  public Link getBaseLink(String url) {



    if (StringUtils.isEmpty(url)) {
      HttpServletRequest request = null;
      request = ((ServletRequestAttributes) RequestContextHolder
          .getRequestAttributes()).getRequest();

      final StringBuffer urlStringBuffer = request.getRequestURL();

      if (!StringUtils.isEmpty(request.getQueryString())) {
        urlStringBuffer.append("?").append(request.getQueryString());
      }

      url = urlStringBuffer.toString();
    }

    final Link link = new Link(url);

    return link;
  }


  public <R extends ResourceSupport> PagedResources<R> toResource(final Page<T> page,
      final ResourceAssembler<T, R> assembler, final String request) {

    return super.toResource(page, assembler, getBaseLink(request));
  }


  @Override
  public <R extends ResourceSupport> PagedResources<R> toResource(final Page<T> page,
      final ResourceAssembler<T, R> assembler) {
    return super.toResource(page, assembler, getBaseLink(""));
  }
}
