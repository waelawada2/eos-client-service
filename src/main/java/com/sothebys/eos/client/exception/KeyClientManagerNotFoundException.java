package com.sothebys.eos.client.exception;

public class KeyClientManagerNotFoundException extends ElementNotFoundException {

  /**
   * Base class for Not found errors.
   *
   * @param id The inexistent id
   */
  public KeyClientManagerNotFoundException(String id) {
    super(id, String.format("Key Client Manager with id '%s' was not found.", id));
  }
}
