package com.sothebys.eos.client.validation.constraint;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import com.sothebys.eos.client.validation.validator.UniqueValueValidator;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;


@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {UniqueValueValidator.class})
public @interface UniqueValue {

  /**
   * Get Field Name.
   * @return String.
   */
  String fieldName();

  /**
   * @return
   */
  String message() default "There is already an existing %s with %s '%s'";
  
  /**
   * @return
   */
  String dbAlias() default "";
  
  /**
   * @return
   */
  Class<?>[] groups() default {};

  /**
   * @return
   */
  Class<? extends Payload>[] payload() default {};
}
