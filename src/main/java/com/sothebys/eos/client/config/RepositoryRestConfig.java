package com.sothebys.eos.client.config;

import com.sothebys.eos.client.model.EntityBase;
import java.util.Set;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.validation.Validator;

@Configuration
public class RepositoryRestConfig extends RepositoryRestConfigurerAdapter {

  @Autowired
  private Validator validator;

  @Override
  public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener v) {
    v.addValidator("beforeCreate", validator);
    v.addValidator("beforeSave", validator);
  }

  @Override
  public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    final Reflections reflections = new Reflections("com.sothebys.eos.client.model");

    final Set<Class<? extends EntityBase>> modelClasses =
        reflections.getSubTypesOf(EntityBase.class);

    for (final Class<?> modelClass : modelClasses) {
      config.exposeIdsFor(modelClass);
    }
  }
}