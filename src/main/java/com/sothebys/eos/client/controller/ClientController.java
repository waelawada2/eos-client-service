package com.sothebys.eos.client.controller;

import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.assembler.ClientAssembler;
import com.sothebys.eos.client.assembler.ParameterizedPagedResourceAssembler;
import com.sothebys.eos.client.dto.ClientDto;
import com.sothebys.eos.client.mapper.ClientMapper;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.service.ClientService;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/clients")
public class ClientController {

  private final ClientService clientService;
  private final ClientMapper clientMapper;
  private final ClientAssembler clientAssembler;

  /**
   * Client Controller constructor.
   *
   * @param clientService Client service implementation.
   * @param clientMapper Client Mapper implementation.
   * @param clientAssembler Client Assembler implementation.
   */
  @Autowired
  public ClientController(ClientService clientService, ClientMapper clientMapper, ClientAssembler
      clientAssembler) {
    this.clientService = clientService;
    this.clientMapper = clientMapper;
    this.clientAssembler = clientAssembler;
  }


  /**
   * Retrieve all Clients based on query params.
   *
   * @param pageable Pageable instance.
   * @param predicate Predicate instance.
   * @param assembler resource assembler instance.
   * @return Pageable Client Dto List
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "The request has been successful"),
      @ApiResponse(code = 500,
          message = "The request has not been successful due to a unexpected server problem"),})
  @GetMapping
  public ResponseEntity<PagedResources<ClientDto>> findAll(final Pageable pageable,
      @RequestParam(value = "projection", required = false) final String projection,
      @QuerydslPredicate(root = Client.class) final Predicate predicate,
      final ParameterizedPagedResourceAssembler<Client> assembler) {

    final Page<Client> clients;

    if (StringUtils.equalsIgnoreCase("idOnly", projection)) {
      clients = clientService.findAllIdOnly(predicate, pageable);
    } else {
      clients = clientService.findAll(predicate, pageable);
    }

    final PagedResources<ClientDto> pagedClients = assembler
        .toResource(clients, this.clientAssembler);

    return ResponseEntity.ok(pagedClients);
  }

  /**
   * Endpoint to Create a client.
   *
   * @param clientDto ClientDto to be created.
   * @return ResponseEntity
   */
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Resource created"),
      @ApiResponse(code = 400, message = "Request is not valid, check response body for details"),
      @ApiResponse(code = 500,
          message = "The request has not been successful due to a unexpected server problem"),})
  @PostMapping
  public ResponseEntity<ClientDto> createClient(
      @Validated(CreateValidation.class) @RequestBody final ClientDto clientDto) {

    final Client clientModel = clientMapper.mapToModel(clientDto);
    final Client clientCreated = clientService.create(clientModel);

    final ClientDto clientDtoResponse = clientAssembler.toResource(clientCreated);

    final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(clientDtoResponse.getEntityId()).toUri();

    return ResponseEntity.created(location).body(clientDtoResponse);

  }

  /**
   * Finds an existing client by the specified id.
   *
   * @param id of the client.
   * @return client object.
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Resource found"),
      @ApiResponse(code = 404, message = "Resource couldn't be found"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @GetMapping("/{id}")
  public ResponseEntity<ClientDto> findById(@PathVariable final String id) {
    final Client storedClient = clientService.findById(id);

    final ClientDto clientDto = clientAssembler.toResource(storedClient);

    return ResponseEntity.ok(clientDto);
  }


  /**
   * Updates an existing client.
   *
   * @param id of the client
   * @param client object
   * @return client object.
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Resource updated"),
      @ApiResponse(code = 400, message = "Request is not valid, check response body for details"),
      @ApiResponse(code = 404, message = "Supplied id is was not found"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @PutMapping("/{id}")
  public ResponseEntity<ClientDto> updateClient(@PathVariable final String id,
      @Validated(UpdateValidation.class) @RequestBody final ClientDto client) {

    final Client persistedClient = clientService.findById(id);

    client.setEntityId(persistedClient.getEntityId());

    final Client clientModel = clientMapper.mapToModel(client);

    final Client updatedClient = clientService.update(clientModel);

    final ClientDto responseClient = clientAssembler.toResource(updatedClient);

    return ResponseEntity.ok(responseClient);
  }


  /**
   * Deletes a client specified by the id.
   *
   * @return no content.
   * @id client id
   */
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Resource successfully deleted"),
      @ApiResponse(code = 404, message = "Supplied id is was not found"),
      @ApiResponse(code = 409, message = "Client could not be deleted"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable final String id) {

    clientService.delete(id);

    return ResponseEntity.noContent().build();
  }

}
