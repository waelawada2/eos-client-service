package com.sothebys.eos.client.controller;

import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.assembler.ParameterizedPagedResourceAssembler;
import com.sothebys.eos.client.assembler.TitleAssembler;
import com.sothebys.eos.client.dto.TitleDto;
import com.sothebys.eos.client.mapper.TitleMapper;
import com.sothebys.eos.client.model.Title;
import com.sothebys.eos.client.service.TitleService;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/titles")
public class TitleController {

  private final TitleService titleService;
  private final TitleMapper titleMapper;
  private final TitleAssembler titleAssembler;

  /**
   * Title Controller constructor.
   *
   * @param titleService Title service implementation.
   * @param titleMapper Title Mapper implementation.
   * @param titleAssembler Title Assembler implementation.
   */
  @Autowired
  public TitleController(TitleService titleService,
      TitleMapper titleMapper,
      TitleAssembler titleAssembler) {
    this.titleService = titleService;
    this.titleMapper = titleMapper;
    this.titleAssembler = titleAssembler;
  }


  /**
   * Titles list.
   *
   * @return Pageable title list
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "The request has been successful"),
      @ApiResponse(code = 500,
          message = "The request has not been successful due to a unexpected server problem"),})
  @GetMapping
  public ResponseEntity<PagedResources<TitleDto>> findAll(final Pageable pageable,
      @QuerydslPredicate(root = Title.class) final Predicate predicate,
      final ParameterizedPagedResourceAssembler<Title> assembler) {

    final Page<Title> titles = titleService
        .findAll(predicate, pageable);

    final PagedResources<TitleDto> pagedTitles = assembler
        .toResource(titles, this.titleAssembler);

    return ResponseEntity.ok(pagedTitles);
  }

  /**
   * Endpoint to Create a title.
   *
   * @param titleDto TitleDto to be created.
   * @return ResponseEntity
   */
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Resource created"),
      @ApiResponse(code = 400, message = "Request is not valid, check response body for details"),
      @ApiResponse(code = 500,
          message = "The request has not been successful due to a unexpected server problem"),})
  @PostMapping
  public ResponseEntity<TitleDto> createTitle(
      @Validated(CreateValidation.class) @RequestBody
      final TitleDto titleDto) {

    final Title titleModel = titleMapper
        .mapToModel(titleDto);
    final Title titleCreated = titleService
        .create(titleModel);

    final TitleDto titleDtoResponse = titleAssembler
        .toResource(titleCreated);

    final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(titleDtoResponse.getEntityId()).toUri();

    return ResponseEntity.created(location).body(titleDtoResponse);

  }

  /**
   * Finds an existing title by the specified id.
   *
   * @param id of the title.
   * @return title object.
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Resource found"),
      @ApiResponse(code = 404, message = "Resource couldn't be found"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @GetMapping("/{id}")
  public ResponseEntity<TitleDto> findById(@PathVariable final Integer id) {
    final Title storedKcm = titleService.findByEntityId(id);

    final TitleDto kcmDto = titleAssembler.toResource(storedKcm);

    return ResponseEntity.ok(kcmDto);
  }


  /**
   * Updates an existing title.
   *
   * @param id of the title
   * @param titleDto object
   * @return title object.
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Resource updated"),
      @ApiResponse(code = 400, message = "Request is not valid, check response body for details"),
      @ApiResponse(code = 404, message = "Supplied id is was not found"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @PutMapping("/{id}")
  public ResponseEntity<TitleDto> updateTitle(@PathVariable final Integer id,
      @Validated(UpdateValidation.class) @RequestBody
      final TitleDto titleDto) {

    final Title persistedTitle = titleService.findByEntityId(id);

    titleDto.setEntityId(persistedTitle.getEntityId());

    final Title titleModel = titleMapper
        .mapToModel(titleDto);

    final Title updatedTitle = titleService
        .update(titleModel);

    final TitleDto responseTitle = titleAssembler
        .toResource(updatedTitle);

    return ResponseEntity.ok(responseTitle);
  }


  /**
   * Deletes a title specified by the id.
   *
   * @return no content.
   * @id title id
   */
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Resource successfully deleted"),
      @ApiResponse(code = 404, message = "Supplied id is was not found"),
      @ApiResponse(code = 409, message = "Title could not be deleted"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable final Integer id) {

    titleService.delete(id);

    return ResponseEntity.noContent().build();
  }

}
