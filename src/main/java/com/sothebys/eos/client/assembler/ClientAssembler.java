package com.sothebys.eos.client.assembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import com.sothebys.eos.client.controller.ClientController;
import com.sothebys.eos.client.controller.KeyClientManagerController;
import com.sothebys.eos.client.dto.ClientDto;
import com.sothebys.eos.client.mapper.ClientMapper;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.repository.ClientStatusRepository;
import com.sothebys.eos.client.repository.ClientTypeRepository;
import com.sothebys.eos.client.repository.GenderRepository;
import com.sothebys.eos.client.repository.MarketingSegmentRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ClientAssembler extends ResourceAssemblerSupport<Client, ClientDto> {

  private final ClientMapper mapper;
  private final RepositoryEntityLinks repositoryEntityLinks;

  @Autowired
  private ClientAssembler(ClientMapper mapper, RepositoryEntityLinks repositoryEntityLinks) {
    super(ClientController.class, ClientDto.class);
    this.repositoryEntityLinks = repositoryEntityLinks;

    this.mapper = mapper;
  }

  @Override
  public List<ClientDto> toResources(Iterable<? extends Client> entities) {
    final List<ClientDto> resources = new ArrayList<>();

    entities.forEach(entity -> resources.add(toResource(entity)));

    return resources;
  }

  @Override
  public ClientDto toResource(Client entity) {
    ClientDto dto = mapper.mapToDto(entity);

    dto.add(linkTo(ClientController.class).slash(dto.getEntityId()).withSelfRel());

    dto.add(linkTo(ClientController.class).slash(dto.getEntityId()).withRel("client"));

    if (dto.getClientStatus() != null) {
      dto.getClientStatus().add(createLink(ClientStatusRepository.class,
          dto.getClientStatus().getEntityId().toString()).withSelfRel());
    }

    if (dto.getClientType() != null) {
      dto.getClientType().add(createLink(ClientTypeRepository.class,
          dto.getClientType().getEntityId().toString()).withSelfRel());
    }

    if (dto.getMarketingSegment() != null) {
      dto.getMarketingSegment().add(createLink(MarketingSegmentRepository.class,
          dto.getMarketingSegment().getEntityId().toString()).withSelfRel());
    }

    if (dto.getMainClient() != null) {
      dto.getMainClient().add(ControllerLinkBuilder.linkTo(ClientController.class).slash(
          dto.getMainClient().getEntityId()).withSelfRel());
    }

    if (dto.getKeyClientManager() != null) {
      dto.getKeyClientManager()
          .add(ControllerLinkBuilder.linkTo(KeyClientManagerController.class).slash(
              dto.getKeyClientManager().getEntityId()).withSelfRel());
    }


    if (dto.getIndividual() != null && dto.getIndividual().getGender() != null) {
      dto.getIndividual().getGender().add(createLink(GenderRepository.class, dto.getIndividual()
          .getGender().getEntityId().toString()).withSelfRel());
    }

    return dto;
  }


  private Link createLink(Class<?> clazz, String id) {
    return repositoryEntityLinks.linkToSingleResource(clazz, id);
  }
}
