package com.sothebys.eos.client.dto.validation;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class ValidationResult {

  private List<FieldMessage> messages = new ArrayList<>();

  public void addFieldError(String path, String message) {
    FieldMessage error = new FieldMessage(path, message);
    messages.add(error);
  }
}
