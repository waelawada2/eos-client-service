package com.sothebys.eos.client.dto.clientsystem;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
public class MessageDto {

  private Header header;
  private JsonNode payload;

  @Data
  public static class Header {

    private String header;
  }
}
