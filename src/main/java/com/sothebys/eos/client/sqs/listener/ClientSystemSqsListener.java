package com.sothebys.eos.client.sqs.listener;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sothebys.eos.client.dto.clientsystem.ClientAssociationUpsertDto;
import com.sothebys.eos.client.dto.clientsystem.ClientUpsertDto;
import com.sothebys.eos.client.dto.clientsystem.MessageDto;
import com.sothebys.eos.client.exception.ClientServiceException;
import com.sothebys.eos.client.exception.InvalidSqsMessageException;
import com.sothebys.eos.client.sqs.support.ClientAssociationHandler;
import com.sothebys.eos.client.sqs.support.ClientHandler;
import java.io.IOException;
import java.util.Map;
import java.util.StringJoiner;
import javax.validation.ConstraintViolationException;
import javax.validation.ElementKind;
import javax.validation.Path;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ClientSystemSqsListener {

  private static final String CREATE_CLIENT_HEADER = "CREATED_CLIENT";
  private static final String UPDATE_CLIENT_HEADER = "UPDATED_CLIENT";
  private static final String UPDATE_CLIENT_ASSOCIATION_HEADER = "UPDATED_CLIENT_ASSOCIATION";
  private static final String DELETE_CLIENT_ASSOCIATION_HEADER = "DELETED_CLIENT_ASSOCIATION";

  private final ObjectMapper objectMapper;
  private final ClientHandler clientHandler;
  private final ClientAssociationHandler clientAssociationHandler;

  /**
   * Constructor.
   */
  @Autowired
  public ClientSystemSqsListener(ObjectMapper objectMapper, ClientHandler clientHandler,
      ClientAssociationHandler clientAssociationHandler) {
    this.objectMapper = objectMapper;
    this.clientHandler = clientHandler;
    this.clientAssociationHandler = clientAssociationHandler;
  }

  /**
   * Listen to client system SQS queue.  The message should be a string with this format:
   * {@code "{"header":{"header":"SOME_HEADER"},"payload":{...}}"}
   */
  @SqsListener(value = "${sqs.client-system-queue}")
  public void queueListener(@Headers Map<String, String> headers) throws IOException {
    log.info("Received an SQS message");

    String message = headers.get("Message");
    if (message == null) {
      throw new InvalidSqsMessageException("SQS message does not contain a Message field");
    }

    String timestamp = headers.get("Timestamp");
    if (timestamp == null) {
      throw new InvalidSqsMessageException("SQS message does not contain a Timestamp field");
    }

    MessageDto messageDto = objectMapper.readValue(message, MessageDto.class);

    if (messageDto.getHeader() == null) {
      throw new InvalidSqsMessageException(
          "SQS message does not have a valid Message (invalid header)");
    }

    String header = messageDto.getHeader().getHeader();

    if (header == null) {
      throw new InvalidSqsMessageException(
          "SQS message does not have a valid Message (invalid header)");
    }

    JsonNode payload = messageDto.getPayload();
    ((ObjectNode) payload).put("lastSyncDate", timestamp);

    log.info("Header: {}", header);
    log.info("Payload: {}", payload);

    switch (header) {
      case CREATE_CLIENT_HEADER:
        clientHandler.createClient(objectMapper.treeToValue(payload, ClientUpsertDto.class));
        break;
      case UPDATE_CLIENT_HEADER:
        clientHandler.updateClient(objectMapper.treeToValue(payload, ClientUpsertDto.class));
        break;
      case UPDATE_CLIENT_ASSOCIATION_HEADER:
        clientAssociationHandler.updateClientAssociation(objectMapper.treeToValue(payload,
            ClientAssociationUpsertDto.class));
        break;
      case DELETE_CLIENT_ASSOCIATION_HEADER:
        JsonNode partyAssociationId = payload.get("partyAssociationId");
        if (partyAssociationId == null) {
          throw new InvalidSqsMessageException(
              "SQS delete client association message does not have a valid payload");
        }
        clientAssociationHandler.deleteClientAssociation(partyAssociationId.asLong());
        break;
      default:
        log.warn("SQS message ignored");
    }

    log.info("Finished processing SQS message");
  }

  @MessageExceptionHandler({InvalidSqsMessageException.class, ClientServiceException.class,
      DataIntegrityViolationException.class})
  public void invalidSqsMessageExceptionHandler(Exception e) {
    log.warn(e.getMessage());
  }

  /**
   * {@link ConstraintViolationException} message exception handler.
   */
  @MessageExceptionHandler
  public void constraintViolationExceptionHandler(ConstraintViolationException ex) {
    log.info("Constraint violation:");
    ex.getConstraintViolations().forEach((violation) -> {
      String propertyName = getPropertyName(violation.getPropertyPath());
      log.info(propertyName + " - " + violation.getMessage());
    });
  }

  @MessageExceptionHandler
  public void exceptionHandler(Exception e) {
    log.error(e.getMessage(), e);
  }

  private String getPropertyName(Path path) {
    StringJoiner propertyNameBuilder = new StringJoiner(".");

    path.spliterator().forEachRemaining(a -> {
      if (a.getKind() == ElementKind.PROPERTY) {
        propertyNameBuilder.add(a.getName());
      }
    });

    return propertyNameBuilder.toString();
  }
}
