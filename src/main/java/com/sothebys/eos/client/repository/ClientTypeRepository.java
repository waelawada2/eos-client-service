package com.sothebys.eos.client.repository;

import com.sothebys.eos.client.model.ClientType;
import com.sothebys.eos.client.model.QClientType;
import com.sothebys.eos.client.repository.support.SimpleCrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "client-types")
public interface ClientTypeRepository extends
    SimpleCrudRepository<ClientType, Integer, QClientType> {

  ClientType findByName(String name);
}
