package com.sothebys.eos.client.config;

import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.not;
import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

  private final String managementContextPath;

  @Autowired
  public SwaggerConfig(@Value("${management.contextPath}") String managementContextPath) {
    this.managementContextPath = managementContextPath;
  }

  /**
   * Configure Springfox Swagger.
   */
  @Bean
  public Docket documentationApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .paths(and(
            not(regex(managementContextPath + ".*")),
            not(regex("/error.*")),
            not(regex("/profile.*"))))
        .build();
  }
}
