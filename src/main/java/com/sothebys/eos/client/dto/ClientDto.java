package com.sothebys.eos.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@JsonInclude(Include.NON_NULL)
@Data
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "clients", value = "client")
public class ClientDto extends ResourceSupport {

  private String entityId;

  private String name;

  private ClientTypeDto clientType;

  private ClientStatusDto clientStatus;

  private KeyClientManagerDto keyClientManager;

  private ClientDto mainClient;

  private String city;

  private String state;

  private String country;

  private String region;

  private MarketingSegmentDto marketingSegment;

  private Integer level;

  private Boolean fromClientSystem;

  private IndividualDto individual;

  private List<ClientAssociationDto> clientAssociations;


}
