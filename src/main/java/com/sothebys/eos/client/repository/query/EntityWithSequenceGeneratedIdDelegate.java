package com.sothebys.eos.client.repository.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.annotations.QueryDelegate;
import com.sothebys.eos.client.model.EntityWithSequenceGeneratedId;
import com.sothebys.eos.client.model.QEntityWithSequenceGeneratedId;

public class EntityWithSequenceGeneratedIdDelegate {

  /**
   * Creates and return a Predicate with all the active entities by the given ID.
   */
  @QueryDelegate(EntityWithSequenceGeneratedId.class)
  public static BooleanBuilder filterEntityById(QEntityWithSequenceGeneratedId entity, String id) {
    return new BooleanBuilder(entity.entityId.eq(id));
  }

}
