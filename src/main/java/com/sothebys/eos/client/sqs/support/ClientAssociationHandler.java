package com.sothebys.eos.client.sqs.support;

import com.sothebys.eos.client.dto.clientsystem.ClientAssociationUpsertDto;
import com.sothebys.eos.client.mapper.ClientAssociationMapper;
import com.sothebys.eos.client.model.ClientAssociation;
import com.sothebys.eos.client.service.ClientAssociationService;
import java.util.Arrays;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Validated
@Slf4j
@Component
public class ClientAssociationHandler {

  private static final int[] ACCEPTED_ASSOCIATION_TYPE_IDS = {42, 112, 113, 122};

  private final ClientAssociationMapper clientAssociationMapper;
  private final ClientAssociationService clientAssociationService;

  @Autowired
  public ClientAssociationHandler(ClientAssociationMapper clientAssociationMapper,
      ClientAssociationService clientAssociationService) {
    this.clientAssociationMapper = clientAssociationMapper;
    this.clientAssociationService = clientAssociationService;
  }

  /**
   * Update a client association.  If the client association does not have an accepted association
   * type id, then don't update the client association.  If the client association doesn't exist,
   * a new client association is created.
   */
  public void updateClientAssociation(@Valid ClientAssociationUpsertDto dto) {
    if (!ArrayUtils.contains(ACCEPTED_ASSOCIATION_TYPE_IDS, dto.getAssociationTypeId())) {
      log.info("Not processing this request because this client association does not have an "
              + "association type id={} that is in the accepted list={}",
          dto.getAssociationTypeId(), Arrays.toString(ACCEPTED_ASSOCIATION_TYPE_IDS));
      return;
    }

    ClientAssociation clientAssociation = clientAssociationMapper.mapToModel(dto);
    clientAssociationService.save(clientAssociation);
  }

  public void deleteClientAssociation(Long partyAssociationId) {
    clientAssociationService.deleteByClientSystemPartyAssociationId(partyAssociationId);
  }
}
