package com.sothebys.eos.client.assembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import com.sothebys.eos.client.controller.TitleController;
import com.sothebys.eos.client.dto.TitleDto;
import com.sothebys.eos.client.mapper.TitleMapper;
import com.sothebys.eos.client.model.Title;
import com.sothebys.eos.client.repository.GenderRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class TitleAssembler extends ResourceAssemblerSupport<Title,
    TitleDto> {

  private final TitleMapper mapper;
  private final RepositoryEntityLinks repositoryEntityLinks;

  @Autowired
  private TitleAssembler(TitleMapper mapper,
      RepositoryEntityLinks repositoryEntityLinks) {
    super(TitleController.class, TitleDto.class);
    this.repositoryEntityLinks = repositoryEntityLinks;

    this.mapper = mapper;
  }

  @Override
  public List<TitleDto> toResources(Iterable<? extends Title> entities) {
    final List<TitleDto> resources = new ArrayList<>();

    entities.forEach(entity -> resources.add(toResource(entity)));

    return resources;
  }

  @Override
  public TitleDto toResource(Title entity) {
    TitleDto dto = mapper.mapToDto(entity);

    dto.add(linkTo(TitleController.class).slash(dto.getEntityId()).withSelfRel());

    dto.add(linkTo(TitleController.class).slash(dto.getEntityId())
        .withRel("title"));

    if (dto.getGender() != null) {
      dto.add(createLink(GenderRepository.class, dto.getGender().getEntityId().toString())
          .withRel("gender"));
    }

    return dto;
  }


  private Link createLink(Class<?> clazz, String id) {
    return repositoryEntityLinks.linkToSingleResource(clazz, id);
  }
}
