package com.sothebys.eos.client.mapper;

import com.sothebys.eos.client.model.AssociationType;
import com.sothebys.eos.client.repository.AssociationTypeRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public class AssociationTypeMapper {

  @Autowired
  private AssociationTypeRepository associationTypeRepository;

  @Named("ClientSystemIdToAssociationType")
  AssociationType mapToModelFromClientSystemId(Integer id) {
    if (id == null) {
      return null;
    }

    String name;

    switch (id) {
      case 42:
        name = "member of";
        break;
      case 112:
        name = "contact for";
        break;
      case 113:
        name = "company director of";
        break;
      case 122:
        name = "deceased member of";
        break;
      default:
        name = null;
    }

    return associationTypeRepository.findByName(name);
  }

}
