package com.sothebys.eos.client.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.BooleanBuilder;
import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.dto.SnsMessageOperation;
import com.sothebys.eos.client.exception.ClientServiceException;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.repository.ClientRepository;
import java.time.Instant;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceImplTest {

  @InjectMocks
  private ClientServiceImpl clientService;

  @Mock
  private ClientRepository clientRepository;

  @Mock
  private MessageServiceImpl messageService;

  @Mock
  private ObjectMapper objectMapper;

  private Client client;
  private Client secondClient;
  private Client clientWithBiggerLastSyncDate;

  @Before
  public void setUp() {
    client = DummyModelHelper.createClientModel();
    secondClient = DummyModelHelper.createClientModel();
    secondClient.setEntityId("456L");
    clientWithBiggerLastSyncDate = DummyModelHelper.createClientModel();
    clientWithBiggerLastSyncDate.setLastSyncDate(Instant.ofEpochMilli(1516656684649L));
  }


  @Test
  public void create() {

    Mockito.when(clientRepository.save(client)).thenReturn(client);
    Mockito.doNothing().when(messageService).sendMessage(anyString(), anyString(),
        any(SnsMessageOperation.class));

    Client response = clientService.create(client);

    assertEquals("12345", response.getEntityId());
    assertEquals("John Smith", response.getName());
    assertEquals(client, response.getMainClient());
    Mockito.verify(messageService, times(1))
        .sendMessage(any(String.class), any(String.class), any(SnsMessageOperation.class));

  }

  @Test
  public void save() {
    Mockito.when(clientRepository.findByEntityId(client.getEntityId()))
        .thenReturn(Optional.of(secondClient));
    Mockito.when(clientRepository.save(client)).thenReturn(client);
    Mockito.doNothing().when(messageService).sendMessage(anyString(), anyString(), any
        (SnsMessageOperation.class));

    Client response = clientService.save(client);

    assertEquals("456L", response.getEntityId());
    assertEquals("John Smith", response.getName());
    assertEquals(client, response.getMainClient());
    Mockito.verify(messageService, times(1))
        .sendMessage(any(String.class), any(String.class), any(SnsMessageOperation.class));
  }

  @Test
  public void saveNoExistingClient() {
    Mockito.when(clientRepository.findByEntityId(client.getEntityId()))
        .thenReturn(Optional.empty());
    Mockito.when(clientRepository.save(client)).thenReturn(client);
    Mockito.doNothing().when(messageService).sendMessage(anyString(), anyString(),
        any(SnsMessageOperation.class));
    Client response = clientService.save(client);

    assertEquals("12345", response.getEntityId());
    assertEquals("John Smith", response.getName());
  }

  @Test(expected = ClientServiceException.class)
  public void saveRequestDateBeforeLastSyncDate() {
    Mockito.when(clientRepository.findByEntityId(client.getEntityId()))
        .thenReturn(Optional.ofNullable(clientWithBiggerLastSyncDate));
    clientService.save(client);
  }

  @Test
  public void findBytExternalId() {

    Mockito.when(clientRepository.findOne(any(BooleanBuilder.class)))
        .thenReturn(client);

    Client response = clientService.findById("EOS-ABC123");

    assertEquals("12345", response.getEntityId());
    assertEquals("John Smith", response.getName());
  }



}