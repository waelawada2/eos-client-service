package com.sothebys.eos.client.service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.exception.ClientAssociationNotFoundException;
import com.sothebys.eos.client.exception.ClientServiceException;
import com.sothebys.eos.client.model.ClientAssociation;
import com.sothebys.eos.client.model.QClientAssociation;
import com.sothebys.eos.client.repository.ClientAssociationRepository;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientAssociationServiceImpl implements ClientAssociationService {

  private final ClientAssociationRepository clientAssociationRepository;
  private final QClientAssociation clientAssociationQuery = QClientAssociation.clientAssociation;
  private final EntityManager em;


  @Autowired
  public ClientAssociationServiceImpl(ClientAssociationRepository clientAssociationRepository,
      EntityManager em) {
    this.clientAssociationRepository = clientAssociationRepository;
    this.em = em;
  }


  @Override
  @Transactional
  public ClientAssociation save(ClientAssociation clientAssoc) {
    Optional<ClientAssociation> existingClientAssocOptional = clientAssociationRepository
        .findByClientSystemPartyAssociationId(clientAssoc.getClientSystemPartyAssociationId());

    existingClientAssocOptional.ifPresent(existingClientAssoc -> {
      clientAssoc.setEntityId(existingClientAssoc.getEntityId());

      if (clientAssoc.getLastSyncDate() != null && existingClientAssoc.getLastSyncDate() != null
          && clientAssoc.getLastSyncDate().compareTo(existingClientAssoc.getLastSyncDate()) < 0) {
        throw new ClientServiceException(String.format("Client association was not saved because "
                + "the request date (%s) is before the stored lastSyncDate (%s)",
            clientAssoc.getLastSyncDate(), existingClientAssoc.getLastSyncDate()));
      }
    });

    return clientAssociationRepository.save(clientAssoc);
  }

  @Override
  @Transactional
  public void deleteByClientSystemPartyAssociationId(Long partyAssociationId) {
    clientAssociationRepository.deleteByClientSystemPartyAssociationId(partyAssociationId);
  }

  @Override
  public ClientAssociation findById(Long id) {
    if (id == null) {
      throw new ClientAssociationNotFoundException(id);
    }

    final ClientAssociation storedClientAssociation =
        clientAssociationRepository.findOne(
            new BooleanBuilder(clientAssociationQuery.entityId.eq(id)));

    if (storedClientAssociation == null) {
      throw new ClientAssociationNotFoundException(id);
    }

    return storedClientAssociation;
  }

  @Override
  @Transactional
  public ClientAssociation create(ClientAssociation clientAssociation) {
    final ClientAssociation createdClientAssociation =
        clientAssociationRepository.save(clientAssociation);

    em.refresh(clientAssociation);

    return createdClientAssociation;
  }

  @Override
  @Transactional
  public ClientAssociation update(ClientAssociation clientAssociation) {
    final ClientAssociation updatedClientAssociation = clientAssociationRepository
        .save(clientAssociation);

    return findById(updatedClientAssociation.getEntityId());
  }

  @Override
  public Page<ClientAssociation> findAll(Predicate predicate, Pageable pageable) {
    Predicate augmentedPredicate = new BooleanBuilder(predicate).getValue();
    return clientAssociationRepository.findAll(augmentedPredicate, pageable);
  }

  @Override
  @Transactional
  public void delete(Long id) {
    clientAssociationRepository.delete(id);


  }
}
