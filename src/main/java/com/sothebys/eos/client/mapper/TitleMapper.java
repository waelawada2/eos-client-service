package com.sothebys.eos.client.mapper;

import com.sothebys.eos.client.dto.TitleDto;
import com.sothebys.eos.client.model.Title;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class TitleMapper {

  public abstract Title mapToModel(TitleDto dto);

  public abstract TitleDto mapToDto(Title model);

}
