package com.sothebys.eos.client.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Sequence implements EntityBase {

  @Id
  private String name;

  private Integer increment;

  @Column(name = "min_value")
  private Integer minValue;

  @Column(name = "max_value")
  private Long maxValue;

  @Column(name = "cur_value")
  private Long curValue;

  private Boolean cycle;
}
