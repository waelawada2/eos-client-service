package com.sothebys.eos.client.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.sothebys.eos.client.dto.SnsMessageOperation;
import com.sothebys.eos.client.exception.MessageServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceImplTest {

  @InjectMocks
  private MessageServiceImpl messageService;

  @Mock
  private AmazonSNS snsClient;

  private PublishResult publishResult;

  @Before
  public void setup() {
    publishResult = new PublishResult();
    publishResult.setMessageId("1");
  }

  @Test(expected = MessageServiceException.class)
  public void testSendMessageFailed() {

    messageService.sendMessage("TEST", "TEST-ARN", SnsMessageOperation.CREATE);

    Mockito.verify(snsClient, times(1))
        .publish(any(PublishRequest.class));

  }


  @Test
  public void testSendMessage() {

    Mockito.when(snsClient.publish(any(PublishRequest.class))).thenReturn(this.publishResult);
    messageService.sendMessage("TEST", "TEST-ARN", SnsMessageOperation.CREATE);

    Mockito.verify(snsClient, times(1))
        .publish(any(PublishRequest.class));

  }
}
