package com.sothebys.eos.client.dto.clientsystem;

import java.time.Instant;
import java.util.Date;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ClientUpsertDto {

  @NotNull
  private Long partyId;

  @NotNull
  private String partyName;

  @NotNull
  private String partyType;

  @NotNull
  private String partyStatus;

  private Long mainClientPartyId;

  private String city;

  private String state;

  private String country;

  private String marketingSegment;

  @NotNull
  private String level;

  private String title;

  private String firstName;

  private String middleName;

  private String lastName;

  private String suffix;

  private String gender;

  private Date birthDate;

  private boolean deceased;

  private Long keyClientManagerId;

  private Instant lastSyncDate;
}
