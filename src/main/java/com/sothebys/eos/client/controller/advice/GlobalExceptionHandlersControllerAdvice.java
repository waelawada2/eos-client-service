package com.sothebys.eos.client.controller.advice;

import com.sothebys.eos.client.exception.ClientServiceException;
import com.sothebys.eos.client.exception.ElementNotFoundException;
import com.sothebys.eos.client.model.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import javax.validation.ConstraintViolationException;
import javax.validation.ElementKind;
import javax.validation.Path;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.core.RepositoryConstraintViolationException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.hateoas.VndErrors;
import org.springframework.hateoas.VndErrors.VndError;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandlersControllerAdvice {

  /**
   * Handler for {@link RepositoryConstraintViolationException} exceptions.
   *
   * @param ex Exception occurred in controller
   * @return Validation errors
   */
  @ResponseBody
  @ExceptionHandler(RepositoryConstraintViolationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ValidationResult repositoryConstraintViolationExceptionHandler(
      RepositoryConstraintViolationException ex) {
    Errors result = ex.getErrors();
    List<FieldError> fieldErrors = result.getFieldErrors();
    return processFieldErrors(fieldErrors);
  }

  @ResponseBody
  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  public VndError dataIntegrityViolationExceptionHandler(DataIntegrityViolationException ex) {
    log.info(ex.getMostSpecificCause().getMessage());
    return new VndError("Data integrity violation", ex.getMostSpecificCause().getMessage());
  }

  @ResponseBody
  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public void resourceNotFoundExceptionHandler(ResourceNotFoundException ex) {
  }


  @ResponseBody
  @ExceptionHandler(EmptyResultDataAccessException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public void emptyResultDataAccessExceptionHandler(EmptyResultDataAccessException ex) {
  }

  @ResponseBody
  @ExceptionHandler(ClientServiceException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public VndError clientServiceExceptionHandler(ClientServiceException ex) {
    return new VndError("Client service error", ex.getMessage());
  }

  @ResponseBody
  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public VndError exceptionHandler(Exception ex) {
    log.error("Uncaught exception", ex);
    return new VndError("Unexpected error", ex.getMessage());
  }

  private ValidationResult processFieldErrors(List<FieldError> fieldErrors) {
    ValidationResult validationResult = new ValidationResult();

    for (FieldError fieldError : fieldErrors) {
      validationResult.addFieldError(fieldError.getField(), fieldError.getDefaultMessage());
    }

    return validationResult;
  }

  /**
   * Handler for {@link MethodArgumentNotValidException} exceptions.
   *
   * @param ex Exception occurred in controller
   * @return Validation errors
   */
  @ResponseBody
  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ValidationResult processValidationError(ConstraintViolationException ex) {
    List<FieldError> fieldErrors = new ArrayList<>();

    ex.getConstraintViolations().forEach((violation) -> {

      String objectName = getObjectName(violation.getLeafBean());
      String propertyName = getPropertyName(violation.getPropertyPath());
      FieldError fieldError = new FieldError(objectName, propertyName, violation.getMessage());
      fieldErrors.add(fieldError);
    });

    return processFieldErrors(fieldErrors);
  }

  private String getObjectName(Object object) {
    String objectName = object.getClass().getSimpleName();
    objectName = StringUtils.uncapitalize(objectName);
    return objectName;
  }

  private String getPropertyName(Path path) {
    StringJoiner propertyNameBuilder = new StringJoiner(".");

    path.spliterator().forEachRemaining(a -> {
      if (a.getKind() == ElementKind.PROPERTY) {
        propertyNameBuilder.add(a.getName());
      }
    });

    return propertyNameBuilder.toString();
  }

  /**
   * Handler for {@link ElementNotFoundException} exceptions.
   *
   * @param ex Exception occurred in controller
   * @return Validation errors
   */
  @ResponseBody
  @ExceptionHandler(ElementNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  VndErrors elementNotFoundExceptionHandler(ElementNotFoundException ex) {
    log.info(ex.getMessage());

    return new VndErrors("Not found", ex.getMessage());
  }

}
