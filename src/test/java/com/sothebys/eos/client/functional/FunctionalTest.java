package com.sothebys.eos.client.functional;


import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ResponseBodyData;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@DirtiesContext
public class FunctionalTest {

  @LocalServerPort
  int port;

  @BeforeClass
  public static void setup() {
    String basePath = System.getProperty("server.base");
    if(StringUtils.isBlank(basePath)) {
      basePath = "eos-client-service/v1/";
    }
    RestAssured.basePath = basePath;

    String baseHost = System.getProperty("server.host");
    if(StringUtils.isBlank(baseHost)) {
      baseHost = "http://localhost";
    }
    RestAssured.baseURI = baseHost;

  }

  @Test
  public void keyCloakTest() {
    RestAssured.port = port;

    ResponseBodyData response =
        given()
        .baseUri("https://keycloakqa.aws.sothebys.com")
        .port(443)
        .contentType(ContentType.URLENC)
        .formParam("grant_type", "password")
        .formParam("username", "testuser@sothebys.com")
        .formParam("password", "test1234")
        .formParam("client_id", "property-database-ui")
        .post("/auth/realms/Core Applications/protocol/openid-connect/token").getBody();

    Assert.assertNotNull(response);
  }

}
