package com.sothebys.eos.client.dto;

import lombok.Data;

@Data
public class SnsMessageDto {

  private String id;

  private String mergedTo;

}
