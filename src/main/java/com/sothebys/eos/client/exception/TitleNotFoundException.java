package com.sothebys.eos.client.exception;

import java.util.Objects;

public class TitleNotFoundException extends ElementNotFoundException {

  /**
   * Base class for Not found errors.
   *
   * @param id The inexistent id
   */
  public TitleNotFoundException(Integer id) {
    super(Objects.toString(id, null), String.format("Title with id '%s' was not found.", id));
  }
}
