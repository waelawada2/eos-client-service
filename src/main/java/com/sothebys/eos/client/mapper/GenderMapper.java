package com.sothebys.eos.client.mapper;

import com.sothebys.eos.client.model.Gender;
import com.sothebys.eos.client.repository.GenderRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public class GenderMapper {

  @Autowired
  private GenderRepository genderRepository;

  @Named("ClientSystemGenderCodeToGender")
  Gender mapToModel(String genderCode) {
    if (genderCode == null) {
      return null;
    }

    String name;

    switch (genderCode.toUpperCase()) {
      case "M":
        name = "Male";
        break;
      case "F":
        name = "Female";
        break;
      default:
        name = null;
    }

    return genderRepository.findByName(name);
  }
}
