package com.sothebys.eos.client.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
public class ClientAssociation implements EntityBase {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private Long entityId;

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @ManyToOne(optional = false)
  private Client client;

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @ManyToOne(optional = false)
  private Client associatedClient;

  @NotNull(groups = {CreateValidation.class, UpdateValidation.class})
  @ManyToOne(optional = false)
  private AssociationType associationType;

  @JsonIgnore
  private Long clientSystemPartyAssociationId;

  @JsonIgnore
  private Instant lastSyncDate;
}
