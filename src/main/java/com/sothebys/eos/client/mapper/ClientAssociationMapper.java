package com.sothebys.eos.client.mapper;

import com.sothebys.eos.client.dto.ClientAssociationDto;
import com.sothebys.eos.client.dto.clientsystem.ClientAssociationUpsertDto;
import com.sothebys.eos.client.model.ClientAssociation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {ClientMapper.class, AssociationTypeMapper.class})
public interface ClientAssociationMapper {

  @Mapping(source = "partyAssociationId", target = "clientSystemPartyAssociationId")
  @Mapping(source = "fromPartyId", target = "associatedClient")
  @Mapping(source = "toPartyId", target = "client")
  @Mapping(source = "associationTypeId", target = "associationType",
      qualifiedByName = "ClientSystemIdToAssociationType")
  ClientAssociation mapToModel(ClientAssociationUpsertDto dto);

  ClientAssociation mapToModel(ClientAssociationDto dto);

  @Mapping(target = "client.clientAssociations", ignore = true)
  @Mapping(target = "associatedClient.clientAssociations", ignore = true)
  @Mapping(target = "client.mainClient", ignore = true)
  @Mapping(target = "associatedClient.mainClient", ignore = true)
  ClientAssociationDto mapToDto(ClientAssociation model);

}
