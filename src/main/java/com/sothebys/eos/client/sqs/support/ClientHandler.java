package com.sothebys.eos.client.sqs.support;

import com.sothebys.eos.client.dto.clientsystem.ClientUpsertDto;
import com.sothebys.eos.client.mapper.ClientMapper;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.service.ClientService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Validated
@Slf4j
@Component
public class ClientHandler {

  private static final String IGNORED_CLIENT_TYPE = "Sotheby's associate";

  private final ClientMapper clientMapper;
  private final ClientService clientService;

  @Autowired
  public ClientHandler(ClientMapper clientMapper, ClientService clientService) {
    this.clientMapper = clientMapper;
    this.clientService = clientService;
  }

  /**
   * Create a client.  If the client has an ignored party type, then don't create the client.
   */
  public void createClient(@Valid ClientUpsertDto dto) {
    if (dto.getPartyType().equals(IGNORED_CLIENT_TYPE)) {
      log.info("Not processing this request because this client has partyType={}",
          dto.getPartyType());
      return;
    }
    Client client = clientMapper.mapToModel(dto);
    clientService.create(client);
  }

  /**
   * Update a client.  If the client has an ignored party type, then don't update the client.
   * If the client doesn't exist, a new client is created.
   */
  public void updateClient(@Valid ClientUpsertDto dto) {
    if (dto.getPartyType().equals(IGNORED_CLIENT_TYPE)) {
      log.info("Not processing this request because this client has partyType={}",
          dto.getPartyType());
      return;
    }
    Client client = clientMapper.mapToModel(dto);
    clientService.save(client);
  }
}
