package com.sothebys.eos.client.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.sothebys.eos.client.model.QTitle;
import com.sothebys.eos.client.model.Title;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TitleRepository extends  PagingAndSortingRepository<Title, Integer>,
    QueryDslPredicateExecutor<Title>,
    QuerydslBinderCustomizer<QTitle> {

  default void customize(QuerydslBindings bindings, QTitle title) {
    bindings.bind(title.name).first(StringExpression::containsIgnoreCase);
    bindings.bind(title.gender.name).first(StringExpression::containsIgnoreCase);
  }
}
