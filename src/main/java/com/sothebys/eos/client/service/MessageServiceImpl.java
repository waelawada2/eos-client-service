package com.sothebys.eos.client.service;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.sothebys.eos.client.dto.SnsMessageOperation;
import com.sothebys.eos.client.exception.MessageServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service("MessageService")
public class MessageServiceImpl implements MessageService {

  private AmazonSNS snsClient;


  @Autowired
  public MessageServiceImpl(@Qualifier("amazonSNS") AmazonSNS snsClient) {
    this.snsClient = snsClient;

  }

  @Override
  @Async("snsMessageTaskExecutor")
  public void sendMessage(String message, String topicArn, SnsMessageOperation operation) {
    log.info("Message Body: " + message);

    final PublishRequest publishRequest = new PublishRequest(topicArn, message);
    publishRequest.setSubject(operation.name());
    final PublishResult publishResult = snsClient.publish(publishRequest);

    if (publishResult == null || StringUtils.isBlank(publishResult.getMessageId())) {
      throw new MessageServiceException("SNS publication failed.");
    }

    log.info("MessageId: " + publishResult.getMessageId());


  }

}
