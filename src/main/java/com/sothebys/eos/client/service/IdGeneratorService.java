package com.sothebys.eos.client.service;

/**
 * Id Generator class.
 */
public interface IdGeneratorService {

  /**
   * Retrieves the right id to use for storing elements in database.
   * @param sequenceName to use.
   * @return generated Id.
   */
  String generateId(String sequenceName);

}
