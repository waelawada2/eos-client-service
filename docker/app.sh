#!/bin/sh

APP_NAME="eos-client-service"

if [ -n "$TAKIPI_MACHINE_NAME" ]; then
  echo "Setting Takipi machine name to $TAKIPI_MACHINE_NAME"
  /opt/takipi/etc/takipi-setup-machine-name "$TAKIPI_MACHINE_NAME"
fi

if [ -n "$TAKIPI_SECRET_KEY" ]; then
  echo "Enabling Takipi Agent with application name ${APP_NAME}"
  TAKIPI_OPTS="-agentlib:TakipiAgent -Dtakipi.name=${APP_NAME}"
fi

java $JAVA_OPTS $TAKIPI_OPTS -jar /app.jar
