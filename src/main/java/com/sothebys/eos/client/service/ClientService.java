package com.sothebys.eos.client.service;

import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

@Validated
public interface ClientService {


  Client findById(String id);

  @Validated(CreateValidation.class)
  Client create(@Valid Client client);

  @Validated(UpdateValidation.class)
  Client update(@Valid Client client);

  @Validated(UpdateValidation.class)
  Client save(@Valid Client client);

  Page<Client> findAll(Predicate predicate, Pageable pageable);

  Page<Client> findAllIdOnly(Predicate predicate, Pageable pageable);


  void delete(final String id);
}
