package com.sothebys.eos.client;

import com.sothebys.eos.client.dto.clientsystem.ClientAssociationUpsertDto;
import com.sothebys.eos.client.dto.clientsystem.ClientUpsertDto;
import com.sothebys.eos.client.model.AssociationType;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.model.ClientAssociation;
import com.sothebys.eos.client.model.ClientStatus;
import com.sothebys.eos.client.model.ClientType;
import com.sothebys.eos.client.model.Gender;
import com.sothebys.eos.client.model.KeyClientManager;
import com.sothebys.eos.client.model.MarketingSegment;
import java.time.Instant;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DummyModelHelper {

  public static Client createClientModel() {
    Client client = new Client();
    client.setEntityId("12345");
    client.setName("John Smith");
    client.setLastSyncDate(Instant.ofEpochMilli(1516656684648L));
    return client;
  }

  public static ClientUpsertDto createClientUpsertDto() {
    ClientUpsertDto clientUpsertDto = new ClientUpsertDto();
    clientUpsertDto.setPartyId(12345L);
    clientUpsertDto.setPartyName("Charles Darwin");
    clientUpsertDto.setPartyType("Individual");
    return clientUpsertDto;
  }

  public static AssociationType createAssociationTypeModel() {
    AssociationType associationType = new AssociationType();
    associationType.setEntityId(123);
    associationType.setName("member of");
    return associationType;
  }

  public static ClientAssociation createClientAssociationModel() {
    Client associatedClient = new Client();
    associatedClient.setEntityId("456");

    ClientAssociation clientAssociation = new ClientAssociation();
    clientAssociation.setEntityId(123L);
    clientAssociation.setClient(createClientModel());
    clientAssociation.setAssociatedClient(associatedClient);
    clientAssociation.setAssociationType(createAssociationTypeModel());
    clientAssociation.setClientSystemPartyAssociationId(646L);
    clientAssociation.setLastSyncDate(Instant.ofEpochMilli(1516656684648L));
    return clientAssociation;
  }

  public static ClientAssociationUpsertDto createClientAssociationUpsertDto() {
    ClientAssociationUpsertDto clientAssociationUpsertDto = new ClientAssociationUpsertDto();
    clientAssociationUpsertDto.setPartyAssociationId(12345L);
    clientAssociationUpsertDto.setFromPartyId(456L);
    clientAssociationUpsertDto.setToPartyId(123L);
    clientAssociationUpsertDto.setAssociationTypeId(42);
    return clientAssociationUpsertDto;
  }

  public static ClientStatus createClientStatusModel() {
    ClientStatus clientStatus = new ClientStatus();
    clientStatus.setEntityId(123);
    clientStatus.setName("Active");
    return clientStatus;
  }

  public static ClientType createClientTypeModel() {
    ClientType clientType = new ClientType();
    clientType.setEntityId(123);
    clientType.setName("Individual");
    return clientType;
  }

  public static Gender createGenderModel() {
    Gender gender = new Gender();
    gender.setEntityId(123);
    gender.setName("Male");
    return gender;
  }

  public static KeyClientManager createKeyClientManagerModel() {
    KeyClientManager keyClientManager = new KeyClientManager();
    keyClientManager.setEntityId("12345");
    keyClientManager.setName("Bob Dylan");
    return keyClientManager;
  }

  public static MarketingSegment createMarketingSegmentModel() {
    MarketingSegment marketingSegment = new MarketingSegment();
    marketingSegment.setEntityId(123);
    marketingSegment.setName("Private Collector");
    return marketingSegment;
  }
}
