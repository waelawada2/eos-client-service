package com.sothebys.eos.client.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum SnsMessageOperation {
  CREATE(),
  DELETE(),
  MERGE()

}