package com.sothebys.eos.client.validation.validator;

import java.lang.annotation.Annotation;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.hibernate.validator.internal.engine.constraintvalidation.ConstraintValidatorContextImpl;

public interface BaseValidator<A extends Annotation, O> extends ConstraintValidator<A, O> {

  Pattern FIELD_NAME_PATTERN = Pattern.compile(".*?\\.arg[0-9]+\\.(.*)");

  /**
   * Method to add and error Message to the ConstraintValidatorContext with parameters.
   * 
   * @param context to add the message.
   * @param field that violates the validation
   * @param messageTemplate Message to be added to the context
   * @param args to add to the message
   */
  default void addErrorMessage(ConstraintValidatorContext context, String field,
      String messageTemplate, Object... args) {
    context.disableDefaultConstraintViolation();
    context.buildConstraintViolationWithTemplate(String.format(messageTemplate, args))
        .addPropertyNode(field).addConstraintViolation();
  }

  /**
   * Infers the field name from the validation context.
   * 
   * @param context for validation
   * @return name of the field being validated
   */
  default String getFieldName(ConstraintValidatorContext context) {
    String fieldName = ((ConstraintValidatorContextImpl) context)
        .getConstraintViolationCreationContexts().get(0).getPath().asString();

    final Matcher matcher = FIELD_NAME_PATTERN.matcher(fieldName);
    if (matcher.find()) {
      fieldName = matcher.group(1);
    }

    return fieldName;
  }
}
