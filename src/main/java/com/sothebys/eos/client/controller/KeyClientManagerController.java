package com.sothebys.eos.client.controller;

import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.assembler.KeyClientManagerAssembler;
import com.sothebys.eos.client.assembler.ParameterizedPagedResourceAssembler;
import com.sothebys.eos.client.dto.KeyClientManagerDto;
import com.sothebys.eos.client.mapper.KeyClientManagerMapper;
import com.sothebys.eos.client.model.KeyClientManager;
import com.sothebys.eos.client.service.KeyClientManagerService;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/key-client-managers")
public class KeyClientManagerController {

  private final KeyClientManagerService keyClientManagerService;
  private final KeyClientManagerMapper keyClientManagerMapper;
  private final KeyClientManagerAssembler keyClientManagerAssembler;

  /**
   * Key Client Manager Controller constructor.
   *
   * @param keyClientManagerService Key Client Manager service implementation.
   * @param keyClientManagerMapper Key Client Manager Mapper implementation.
   * @param keyClientManagerAssembler Key Client Manager Assembler implementation.
   */
  @Autowired
  public KeyClientManagerController(KeyClientManagerService keyClientManagerService,
      KeyClientManagerMapper keyClientManagerMapper,
      KeyClientManagerAssembler keyClientManagerAssembler) {
    this.keyClientManagerService = keyClientManagerService;
    this.keyClientManagerMapper = keyClientManagerMapper;
    this.keyClientManagerAssembler = keyClientManagerAssembler;
  }


  /**
   * KeyClientManagers list.
   *
   * @return Pageable keyClientManager list
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "The request has been successful"),
      @ApiResponse(code = 500,
          message = "The request has not been successful due to a unexpected server problem"),})
  @GetMapping
  public ResponseEntity<PagedResources<KeyClientManagerDto>> findAll(final Pageable pageable,
      @QuerydslPredicate(root = KeyClientManager.class) final Predicate predicate,
      final ParameterizedPagedResourceAssembler<KeyClientManager> assembler) {

    final Page<KeyClientManager> keyClientManagers = keyClientManagerService
        .findAll(predicate, pageable);

    final PagedResources<KeyClientManagerDto> pagedKeyClientManagers = assembler
        .toResource(keyClientManagers, this.keyClientManagerAssembler);

    return ResponseEntity.ok(pagedKeyClientManagers);
  }

  /**
   * Endpoint to Create a Key Client Manager.
   *
   * @param keyClientManagerDto to be created.
   * @return ResponseEntity
   */
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Resource created"),
      @ApiResponse(code = 400, message = "Request is not valid, check response body for details"),
      @ApiResponse(code = 500,
          message = "The request has not been successful due to a unexpected server problem"),})
  @PostMapping
  public ResponseEntity<KeyClientManagerDto> createKeyClientManager(
      @Validated(CreateValidation.class)
      @RequestBody final KeyClientManagerDto keyClientManagerDto) {

    final KeyClientManager keyClientManagerModel = keyClientManagerMapper
        .mapToModel(keyClientManagerDto);
    final KeyClientManager keyClientManagerCreated = keyClientManagerService
        .create(keyClientManagerModel);

    final KeyClientManagerDto keyClientManagerDtoResponse = keyClientManagerAssembler
        .toResource(keyClientManagerCreated);

    final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(keyClientManagerDtoResponse.getEntityId()).toUri();

    return ResponseEntity.created(location).body(keyClientManagerDtoResponse);

  }

  /**
   * Finds an existing Key Client Manager by the specified id.
   *
   * @param id of the client.
   * @return Key Client Manager object.
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Resource found"),
      @ApiResponse(code = 404, message = "Resource couldn't be found"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @GetMapping("/{id}")
  public ResponseEntity<KeyClientManagerDto> findById(@PathVariable final String id) {
    final KeyClientManager storedKcm = keyClientManagerService.findByEntityId(id);

    final KeyClientManagerDto kcmDto = keyClientManagerAssembler.toResource(storedKcm);

    return ResponseEntity.ok(kcmDto);
  }


  /**
   * Updates an existing Key Client Manager.
   *
   * @param id of the Key Client Manager
   * @param keyClientManagerDto object
   * @return Key Client Manager object.
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Resource updated"),
      @ApiResponse(code = 400, message = "Request is not valid, check response body for details"),
      @ApiResponse(code = 404, message = "Supplied id is was not found"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @PutMapping("/{id}")
  public ResponseEntity<KeyClientManagerDto> updateClient(@PathVariable final String id,
      @Validated(UpdateValidation.class)
      @RequestBody final KeyClientManagerDto keyClientManagerDto) {

    final KeyClientManager persistedClient = keyClientManagerService.findByEntityId(id);

    keyClientManagerDto.setEntityId(persistedClient.getEntityId());

    final KeyClientManager keyClientManagerModel = keyClientManagerMapper
        .mapToModel(keyClientManagerDto);

    final KeyClientManager updatedKeyClientManager = keyClientManagerService
        .update(keyClientManagerModel);

    final KeyClientManagerDto responseKeyClientManager = keyClientManagerAssembler
        .toResource(updatedKeyClientManager);

    return ResponseEntity.ok(responseKeyClientManager);
  }


  /**
   * Deletes a Key Client Manager specified by the id.
   *
   * @return no content.
   * @id Key Client Manager id
   */
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Resource successfully deleted"),
      @ApiResponse(code = 404, message = "Supplied id is was not found"),
      @ApiResponse(code = 409, message = "KeyClientManager could not be deleted"),
      @ApiResponse(code = 500,
          message = "The request has not been successful due to a unexpected server problem"),})
  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable final String id) {

    keyClientManagerService.delete(id);

    return ResponseEntity.noContent().build();
  }

}
