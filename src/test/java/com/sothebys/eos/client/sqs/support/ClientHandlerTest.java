package com.sothebys.eos.client.sqs.support;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.dto.clientsystem.ClientUpsertDto;
import com.sothebys.eos.client.mapper.ClientMapper;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.service.ClientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClientHandlerTest {

  @InjectMocks
  private ClientHandler clientHandler;

  @Mock
  private ClientMapper clientMapper;

  @Mock
  private ClientService clientService;

  private Client client;
  private ClientUpsertDto clientUpsertDto;
  private ClientUpsertDto clientUpsertDtoIgnoredType;

  @Before
  public void setUp() {
    client = DummyModelHelper.createClientModel();
    clientUpsertDto = DummyModelHelper.createClientUpsertDto();
    clientUpsertDtoIgnoredType = DummyModelHelper.createClientUpsertDto();
    clientUpsertDtoIgnoredType.setPartyType("Sotheby's associate");
  }

  @Test
  public void createClient() {
    Mockito.when(clientMapper.mapToModel(clientUpsertDto)).thenReturn(client);
    Mockito.when(clientService.create(client)).thenReturn(client);

    clientHandler.createClient(clientUpsertDto);

    Mockito.verify(clientMapper).mapToModel(clientUpsertDto);
    Mockito.verify(clientService).create(client);
  }

  @Test
  public void createClientIgnoredType() {
    clientHandler.createClient(clientUpsertDto);

    Mockito.verify(clientMapper, Mockito.never()).mapToModel(clientUpsertDtoIgnoredType);
    Mockito.verify(clientService, Mockito.never()).create(client);
  }

  @Test
  public void updateClient() {
    Mockito.when(clientMapper.mapToModel(clientUpsertDto)).thenReturn(client);
    Mockito.when(clientService.save(client)).thenReturn(client);

    clientHandler.updateClient(clientUpsertDto);

    Mockito.verify(clientMapper).mapToModel(clientUpsertDto);
    Mockito.verify(clientService).save(client);
  }

  @Test
  public void updateClientIgnoredType() {
    clientHandler.updateClient(clientUpsertDto);

    Mockito.verify(clientMapper, Mockito.never()).mapToModel(clientUpsertDtoIgnoredType);
    Mockito.verify(clientService, Mockito.never()).save(client);
  }
}