package com.sothebys.eos.client.repository.support;

import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.model.EntityWithSequenceGeneratedId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CustomRepository<T extends EntityWithSequenceGeneratedId> {
  <S extends T> S insertWithIdGeneration(S entity);

  /**
   * Returns a {@link org.springframework.data.domain.Page} of entities This also uses provided
   * projections ( can be JavaBean or constructor or anything supported by QueryDSL.
   *
   * @param factoryExpression this constructor expression will be used for transforming query
   *        results.
   * @param predicate predicate instance.
   * @param pageable pageable instance.
   */
  Page<T> findAll(FactoryExpression<T> factoryExpression, Predicate predicate, Pageable pageable);
}
