package com.sothebys.eos.client.mapper;

import com.sothebys.eos.client.dto.ClientDto;
import com.sothebys.eos.client.dto.clientsystem.ClientUpsertDto;
import com.sothebys.eos.client.exception.ClientNotFoundException;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.service.ClientService;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring", uses = {ClientTypeMapper.class, ClientStatusMapper.class,
    MarketingSegmentMapper.class, GenderMapper.class, KeyClientManagerMapper.class,
    ClientAssociationMapper.class})
public abstract class ClientMapper {

  @Autowired
  private ClientService clientService;

  @Mapping(source = "partyId", target = "entityId")
  @Mapping(source = "partyName", target = "name")
  @Mapping(source = "partyType", target = "clientType")
  @Mapping(source = "partyStatus", target = "clientStatus")
  @Mapping(source = "mainClientPartyId", target = "mainClient",
      qualifiedByName = "LongIdToClientNullIfNotFound")
  @Mapping(source = "title", target = "individual.title")
  @Mapping(source = "firstName", target = "individual.firstName")
  @Mapping(source = "middleName", target = "individual.middleName")
  @Mapping(source = "lastName", target = "individual.lastName")
  @Mapping(source = "suffix", target = "individual.suffix")
  @Mapping(source = "gender", target = "individual.gender",
      qualifiedByName = "ClientSystemGenderCodeToGender")
  @Mapping(source = "birthDate", target = "individual.birthDate")
  @Mapping(source = "deceased", target = "individual.deceased")
  @Mapping(source = "keyClientManagerId", target = "keyClientManager")
  @Mapping(constant = "true", target = "fromClientSystem")
  public abstract Client mapToModel(ClientUpsertDto dto);


  public abstract Client mapToModel(ClientDto dto);


  Client mapToModel(Long id) {
    return clientService.findById(id != null ? String.valueOf(id) : null);
  }

  @Named("LongIdToClientNullIfNotFound")
  Client mapToModelNullIfNotFound(Long id) {
    try {
      return clientService.findById(id != null ? String.valueOf(id) : null);
    } catch (ClientNotFoundException e) {
      return null;
    }
  }

  @Mapping(target = "mainClient.mainClient", ignore = true)
  public abstract ClientDto mapToDto(Client model);

  @Mapping(source = "level", target = "level")
  int mapLevelStringToInt(String level) {
    String levelWithoutPrefix = StringUtils.removeStartIgnoreCase(level, "Level ");
    return Integer.parseInt(levelWithoutPrefix);
  }

}
