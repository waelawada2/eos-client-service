package com.sothebys.eos.client.repository.support;

import com.sothebys.eos.client.service.IdGeneratorService;
import java.io.Serializable;
import javax.persistence.EntityManager;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.core.RepositoryInformation;

public class CustomRepositoryFactory<T, I extends Serializable> extends JpaRepositoryFactory {

  private final IdGeneratorService idGeneratorService;
  private EntityManager entityManager;

  /**
   * Constructor.
   * @param entityManager instance.
   * @param idGeneratorService instance.
   */
  public CustomRepositoryFactory(EntityManager entityManager,
      IdGeneratorService idGeneratorService) {
    super(entityManager);
    this.entityManager = entityManager;
    this.idGeneratorService = idGeneratorService;
  }

  @Override
  protected Object getTargetRepository(RepositoryInformation information) {
    Object targetRepository = super.getTargetRepository(information);
    if (targetRepository instanceof CustomRepositoryImpl) {
      ((CustomRepositoryImpl<?,?>) targetRepository).setIdGeneratorService(idGeneratorService);
    }
    return targetRepository;
  }
}
