package com.sothebys.eos.client.repository;

import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.StringExpression;
import com.sothebys.eos.client.model.Client;
import com.sothebys.eos.client.model.QClient;
import com.sothebys.eos.client.repository.support.CustomRepository;
import java.time.LocalDate;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends PagingAndSortingRepository<Client, String>,
    JpaRepository<Client, String>,
    QueryDslPredicateExecutor<Client>, QuerydslBinderCustomizer<QClient>, CustomRepository<Client> {

  /**
   * QueryDSL customization.
   *
   * @param bindings bindings
   * @param client client
   */
  default void customize(QuerydslBindings bindings, QClient client) {
    bindings.bind(client.name).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.keyClientManager.name).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.mainClient.name).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.city).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.state).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.country).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.region).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.individual.title).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.individual.firstName).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.individual.middleName).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.individual.lastName).first(StringExpression::containsIgnoreCase);
    bindings.bind(client.individual.suffix).first(StringExpression::containsIgnoreCase);

    bindings.bind(client.individual.birthDate).as("agedate").first(
        (DatePath<LocalDate> path, LocalDate ageDate) -> {

          LocalDate from = ageDate.minusYears(1).plusDays(1);

          return path.between(from, ageDate);
        });
  }



  Optional<Client> findByEntityId(String externalId);
}
