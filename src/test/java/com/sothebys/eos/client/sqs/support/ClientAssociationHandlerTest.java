package com.sothebys.eos.client.sqs.support;

import com.sothebys.eos.client.DummyModelHelper;
import com.sothebys.eos.client.dto.clientsystem.ClientAssociationUpsertDto;
import com.sothebys.eos.client.mapper.ClientAssociationMapper;
import com.sothebys.eos.client.model.ClientAssociation;
import com.sothebys.eos.client.service.ClientAssociationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClientAssociationHandlerTest {

  @InjectMocks
  private ClientAssociationHandler clientAssociationHandler;

  @Mock
  private ClientAssociationMapper clientAssociationMapper;

  @Mock
  private ClientAssociationService clientAssociationService;

  private ClientAssociation clientAssociation;
  private ClientAssociationUpsertDto clientAssociationUpsertDto;
  private ClientAssociationUpsertDto clientAssociationUpsertDtoIgnoredType;

  @Before
  public void setUp() {
    clientAssociation = DummyModelHelper.createClientAssociationModel();
    clientAssociationUpsertDto = DummyModelHelper.createClientAssociationUpsertDto();
    clientAssociationUpsertDtoIgnoredType = DummyModelHelper.createClientAssociationUpsertDto();
    clientAssociationUpsertDtoIgnoredType.setAssociationTypeId(999);
  }

  @Test
  public void updateClientAssociation() {
    Mockito.when(clientAssociationMapper.mapToModel(clientAssociationUpsertDto))
        .thenReturn(clientAssociation);
    Mockito.when(clientAssociationService.save(clientAssociation)).thenReturn(clientAssociation);

    clientAssociationHandler.updateClientAssociation(clientAssociationUpsertDto);

    Mockito.verify(clientAssociationMapper).mapToModel(clientAssociationUpsertDto);
    Mockito.verify(clientAssociationService).save(clientAssociation);
  }

  @Test
  public void updateClientAssociationIgnoredType() {
    clientAssociationHandler.updateClientAssociation(clientAssociationUpsertDtoIgnoredType);

    Mockito.verify(clientAssociationMapper, Mockito.never()).mapToModel(clientAssociationUpsertDto);
    Mockito.verify(clientAssociationService, Mockito.never()).save(clientAssociation);
  }

  @Test
  public void deleteClientAssociation() {
    Mockito.doNothing().when(clientAssociationService).deleteByClientSystemPartyAssociationId(123L);
    clientAssociationHandler.deleteClientAssociation(123L);
    Mockito.verify(clientAssociationService).deleteByClientSystemPartyAssociationId(123L);
  }
}