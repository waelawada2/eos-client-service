package com.sothebys.eos.client.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@Data
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "keyClientManagers", value = "keyClientManager")
public class KeyClientManagerDto extends ResourceSupport {

  private String entityId;

  private String name;

}
