package com.sothebys.eos.client.controller;

import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.assembler.ClientAssociationAssembler;
import com.sothebys.eos.client.assembler.ParameterizedPagedResourceAssembler;
import com.sothebys.eos.client.dto.ClientAssociationDto;
import com.sothebys.eos.client.mapper.ClientAssociationMapper;
import com.sothebys.eos.client.model.ClientAssociation;
import com.sothebys.eos.client.service.ClientAssociationService;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/client-associations")
public class ClientAssociationController {


  private final ClientAssociationService clientAssociationService;
  private final ClientAssociationMapper clientAssociationMapper;
  private final ClientAssociationAssembler clientAssociationAssembler;

  /**
   * Client Association Controller constructor.
   *
   * @param clientAssociationService Client Association service implementation.
   * @param clientAssociationMapper Client Association Mapper implementation.
   * @param clientAssociationAssembler Client Association Assembler implementation.
   */
  @Autowired
  public ClientAssociationController(ClientAssociationService clientAssociationService,
      ClientAssociationMapper clientAssociationMapper,
      ClientAssociationAssembler clientAssociationAssembler) {
    this.clientAssociationService = clientAssociationService;
    this.clientAssociationMapper = clientAssociationMapper;
    this.clientAssociationAssembler = clientAssociationAssembler;
  }


  /**
   * Client Association list.
   *
   * @return Pageable Client Associations list
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "The request has been successful"),
      @ApiResponse(code = 500,
          message = "The request has not been successful due to a unexpected server problem"),})
  @GetMapping
  public ResponseEntity<PagedResources<ClientAssociationDto>> findAll(final Pageable pageable,
      @QuerydslPredicate(root = ClientAssociation.class) final Predicate predicate,
      final ParameterizedPagedResourceAssembler<ClientAssociation> assembler) {

    final Page<ClientAssociation> events = clientAssociationService.findAll(predicate, pageable);

    final PagedResources<ClientAssociationDto> pagedClientAssociations = assembler
        .toResource(events, this.clientAssociationAssembler);

    return ResponseEntity.ok(pagedClientAssociations);
  }

  /**
   * Endpoint to Create a clientAssociation.
   *
   * @param clientAssociationDto ClientAssociationDto to be created.
   * @return ResponseEntity
   */
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Resource created"),
      @ApiResponse(code = 400, message = "Request is not valid, check response body for details"),
      @ApiResponse(code = 500,
          message = "The request has not been successful due to a unexpected server problem"),})
  @PostMapping
  public ResponseEntity<ClientAssociationDto> createClient(
      @Validated(CreateValidation.class) @RequestBody
      final ClientAssociationDto clientAssociationDto) {

    final ClientAssociation clientAssociationModel = clientAssociationMapper
        .mapToModel(clientAssociationDto);

    final ClientAssociation clientAssociationCreated = clientAssociationService
        .create(clientAssociationModel);

    final ClientAssociationDto clientAssociationDtoResponse = clientAssociationAssembler
        .toResource(clientAssociationCreated);

    final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(clientAssociationDtoResponse.getEntityId()).toUri();

    return ResponseEntity.created(location).body(clientAssociationDtoResponse);

  }

  /**
   * Finds an existing client association by the specified id.
   *
   * @param id of the client association.
   * @return client association object.
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Resource found"),
      @ApiResponse(code = 404, message = "Resource couldn't be found"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @GetMapping("/{id}")
  public ResponseEntity<ClientAssociationDto> findById(@PathVariable final Long id) {
    final ClientAssociation storedClient = clientAssociationService.findById(id);

    final ClientAssociationDto clientAssociationDto = clientAssociationAssembler
        .toResource(storedClient);

    return ResponseEntity.ok(clientAssociationDto);
  }


  /**
   * Updates an existing client association.
   *
   * @param id of the client association
   * @param clientAssociationDto object
   * @return client object.
   */
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Resource updated"),
      @ApiResponse(code = 400, message = "Request is not valid, check response body for details"),
      @ApiResponse(code = 404, message = "Supplied id is was not found"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @PutMapping("/{id}")
  public ResponseEntity<ClientAssociationDto> updateClient(@PathVariable final Long id,
      @Validated(UpdateValidation.class) @RequestBody
      final ClientAssociationDto clientAssociationDto) {

    final ClientAssociation persistedClientAssociation = clientAssociationService.findById(id);

    clientAssociationDto.setEntityId(persistedClientAssociation.getEntityId());

    final ClientAssociation clientAssociationModel = clientAssociationMapper
        .mapToModel(clientAssociationDto);

    final ClientAssociation updatedClientAssociation = clientAssociationService
        .update(clientAssociationModel);

    final ClientAssociationDto responseClientAssociation = clientAssociationAssembler
        .toResource(updatedClientAssociation);

    return ResponseEntity.ok(responseClientAssociation);
  }


  /**
   * Deletes a client association specified by the id.
   *
   * @return no content.
   * @id client association id
   */
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Resource successfully deleted"),
      @ApiResponse(code = 404, message = "Supplied id is was not found"),
      @ApiResponse(code = 409, message = "Event could not be deleted"), @ApiResponse(code = 500,
      message = "The request has not been successful due to a unexpected server problem"),})
  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable final Long id) {

    clientAssociationService.delete(id);

    return ResponseEntity.noContent().build();
  }

}
