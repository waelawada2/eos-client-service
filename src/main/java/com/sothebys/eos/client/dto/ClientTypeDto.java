package com.sothebys.eos.client.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;

@Data
@EqualsAndHashCode(callSuper = false)
public class ClientTypeDto extends ResourceSupport {

  private Integer entityId;

  private String name;
}
