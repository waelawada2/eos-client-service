package com.sothebys.eos.client.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@Data
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "titles", value = "title")
public class TitleDto extends ResourceSupport {

  private Integer entityId;

  private String name;

  private GenderDto gender;
}
