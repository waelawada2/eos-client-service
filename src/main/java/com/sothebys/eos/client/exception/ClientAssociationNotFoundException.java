package com.sothebys.eos.client.exception;

import java.util.Objects;

public class ClientAssociationNotFoundException extends ElementNotFoundException {

  /**
   * Base class for Not found errors.
   *
   * @param id The inexistent id
   */
  public ClientAssociationNotFoundException(Long id) {
    super(Objects.toString(id, null), String.format("Client Association with id '%s' was not "
            + "found.",
        id));
  }
}
