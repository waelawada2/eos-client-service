package com.sothebys.eos.client.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.sothebys.eos.client.model.QSuffix;
import com.sothebys.eos.client.model.Suffix;
import com.sothebys.eos.client.repository.support.SimpleCrudRepository;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface SuffixRepository extends SimpleCrudRepository<Suffix, Integer, QSuffix> {

  default void customize(QuerydslBindings bindings, QSuffix suffix) {
    bindings.bind(suffix.name).first(StringExpression::containsIgnoreCase);
  }
}
