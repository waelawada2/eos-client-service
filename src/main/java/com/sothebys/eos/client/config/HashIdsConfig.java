package com.sothebys.eos.client.config;

import org.hashids.Hashids;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HashIdsConfig {

  @Bean
  public Hashids hashids() {
    return new Hashids("sothebys", 6, "BCDFGHJKLMNPQRSTVWXYZ1234567890");
  }
}
