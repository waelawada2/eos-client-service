# EOS Client Service

This service exposes REST resources for the management of eos clients.

## Setup locally
1. Run a local MySQL server with user/password as root/root.  Note: if you already have a local instance running, but with a different user/password, just change the config in application.yml.
2. Build and run the client service using maven: `mvn clean install spring-boot:run`.  Note: when you do this, your clientdb database will get filled with table schemas and initial data set.

## Test locally using Postman
1. Import the v2.1 collection found under src/test/resources/postman.
2. Import the local envrionment found under src/test/resources/postman/env.
3. Test using Postman.  If you'd like, you can view the swagger definitions at http://localhost:9970/eos-client-service/v1/swagger-ui.html