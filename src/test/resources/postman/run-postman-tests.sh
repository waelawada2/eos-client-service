#!/bin/sh
WORK_DIR="$( dirname "${BASH_SOURCE[0]}" )"
APP_NAME=eos-client-service

newman run ${WORK_DIR}/stb-${APP_NAME}-tests.postman_collection.json -e ${WORK_DIR}/env/stb-${APP_NAME}-local.postman_environment.json --reporters cli -k
