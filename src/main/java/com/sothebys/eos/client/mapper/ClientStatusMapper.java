package com.sothebys.eos.client.mapper;

import com.sothebys.eos.client.model.ClientStatus;
import com.sothebys.eos.client.repository.ClientStatusRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public class ClientStatusMapper {

  @Autowired
  private ClientStatusRepository clientStatusRepository;

  ClientStatus mapToModel(String name) {
    return clientStatusRepository.findByName(name);
  }
}
