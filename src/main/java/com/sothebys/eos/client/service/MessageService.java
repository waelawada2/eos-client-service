package com.sothebys.eos.client.service;

import com.sothebys.eos.client.dto.SnsMessageOperation;

public interface MessageService {
  void sendMessage(String message, String topicArn, SnsMessageOperation operation);
}
