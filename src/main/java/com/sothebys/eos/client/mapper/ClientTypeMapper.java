package com.sothebys.eos.client.mapper;

import com.sothebys.eos.client.model.ClientType;
import com.sothebys.eos.client.repository.ClientTypeRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public class ClientTypeMapper {

  @Autowired
  private ClientTypeRepository clientTypeRepository;

  ClientType mapToModel(String name) {
    return clientTypeRepository.findByName(name);
  }
}
