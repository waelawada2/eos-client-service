package com.sothebys.eos.client.service;

import com.querydsl.core.types.Predicate;
import com.sothebys.eos.client.model.KeyClientManager;
import com.sothebys.eos.client.validation.group.CreateValidation;
import com.sothebys.eos.client.validation.group.UpdateValidation;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

@Validated
public interface KeyClientManagerService {

  KeyClientManager findByEntityId(String id);

  Page<KeyClientManager> findAll(Predicate predicate, Pageable pageable);

  @Validated(CreateValidation.class)
  KeyClientManager create(@Valid KeyClientManager client);

  @Validated(UpdateValidation.class)
  KeyClientManager update(@Valid KeyClientManager client);

  void delete(final String id);
}
