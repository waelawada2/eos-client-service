package com.sothebys.eos.client.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
public class ClientType implements EntityBase {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private Integer entityId;

  @NotNull
  @Column(nullable = false)
  private String name;
}
