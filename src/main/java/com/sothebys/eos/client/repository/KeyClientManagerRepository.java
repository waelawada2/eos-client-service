package com.sothebys.eos.client.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.sothebys.eos.client.model.KeyClientManager;
import com.sothebys.eos.client.model.QKeyClientManager;
import com.sothebys.eos.client.repository.support.CustomRepository;
import java.util.Optional;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KeyClientManagerRepository extends
    PagingAndSortingRepository<KeyClientManager, String>,
    QueryDslPredicateExecutor<KeyClientManager>,
    QuerydslBinderCustomizer<QKeyClientManager>, CustomRepository<KeyClientManager> {

  Optional<KeyClientManager> findByEntityId(String externalId);

  default void customize(QuerydslBindings bindings, QKeyClientManager keyClientManager) {
    bindings.bind(keyClientManager.name).first(StringExpression::containsIgnoreCase);
  }
}
